-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 12, 2020 at 01:37 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ums`
--

-- --------------------------------------------------------

--
-- Table structure for table `item_master`
--

CREATE TABLE `item_master` (
  `im_id` int(11) NOT NULL,
  `im_name` varchar(255) NOT NULL,
  `im_item_id` int(11) NOT NULL,
  `im_qty` int(11) NOT NULL,
  `im_amout` float NOT NULL,
  `im_total_amout` double NOT NULL,
  `im_order_id` int(11) NOT NULL,
  `im_inserted_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu_master`
--

CREATE TABLE `menu_master` (
  `menu_id` int(11) NOT NULL,
  `menu_name` varchar(255) NOT NULL,
  `menu_price` varchar(15) NOT NULL,
  `menu_rd_id` int(11) NOT NULL,
  `menu_inserted` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_master`
--

CREATE TABLE `order_master` (
  `om_id` int(11) NOT NULL,
  `om_date` date NOT NULL,
  `om_table_id` int(11) NOT NULL,
  `om_table_name` varchar(255) NOT NULL,
  `om_customer_name` varchar(255) NOT NULL,
  `om_mobile` varchar(15) NOT NULL,
  `om_total_amount` double NOT NULL,
  `om_total_item` int(11) NOT NULL,
  `om_rd_id` int(11) NOT NULL,
  `om_status` varchar(200) NOT NULL,
  `om_user_id` int(11) NOT NULL,
  `om_user_name` varchar(255) NOT NULL,
  `om_inserted_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_detail`
--

CREATE TABLE `restaurant_detail` (
  `rd_id` int(11) NOT NULL,
  `rd_restaurant_name` varchar(255) NOT NULL,
  `rd_restaurant_owner_name` varchar(255) NOT NULL,
  `rd_restaurant_email` varchar(255) NOT NULL,
  `rd_mobile_number` int(11) NOT NULL,
  `rd_address` text NOT NULL,
  `rd_inserted_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restaurant_detail`
--

INSERT INTO `restaurant_detail` (`rd_id`, `rd_restaurant_name`, `rd_restaurant_owner_name`, `rd_restaurant_email`, `rd_mobile_number`, `rd_address`, `rd_inserted_date`) VALUES
(0, 'Super admin', 'admin', 'admin@admin.com', 0, 'admin', '2020-07-20 23:21:45');

-- --------------------------------------------------------

--
-- Table structure for table `role_master`
--

CREATE TABLE `role_master` (
  `id` int(11) NOT NULL,
  `restaurant_id` int(11) NOT NULL,
  `admin_is` varchar(50) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `p_delete` tinyint(4) NOT NULL DEFAULT 0,
  `p_edit` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role_master`
--

INSERT INTO `role_master` (`id`, `restaurant_id`, `admin_is`, `user_id`, `p_delete`, `p_edit`) VALUES
(9, 0, 'superadmin', 3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_master`
--

CREATE TABLE `table_master` (
  `tm_id` int(11) NOT NULL,
  `tm_name` varchar(255) NOT NULL,
  `tm_rd_id` int(11) NOT NULL,
  `inserted_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_master`
--

CREATE TABLE `user_master` (
  `um_id` int(11) NOT NULL,
  `um_name` varchar(255) NOT NULL,
  `um_email` varchar(255) NOT NULL,
  `um_mobile` varchar(15) NOT NULL,
  `um_password` text NOT NULL,
  `um_rd_id` int(10) NOT NULL,
  `um_type` varchar(100) NOT NULL,
  `um_inserted_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_master`
--

INSERT INTO `user_master` (`um_id`, `um_name`, `um_email`, `um_mobile`, `um_password`, `um_rd_id`, `um_type`, `um_inserted_date`) VALUES
(3, 'admin', 'admin@admin.com', '909090909', '21232f297a57a5a743894a0e4a801fc3', 0, 'superadmin', '2020-07-10 12:57:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `item_master`
--
ALTER TABLE `item_master`
  ADD PRIMARY KEY (`im_id`);

--
-- Indexes for table `menu_master`
--
ALTER TABLE `menu_master`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `order_master`
--
ALTER TABLE `order_master`
  ADD PRIMARY KEY (`om_id`);

--
-- Indexes for table `restaurant_detail`
--
ALTER TABLE `restaurant_detail`
  ADD PRIMARY KEY (`rd_id`);

--
-- Indexes for table `role_master`
--
ALTER TABLE `role_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_master`
--
ALTER TABLE `table_master`
  ADD PRIMARY KEY (`tm_id`);

--
-- Indexes for table `user_master`
--
ALTER TABLE `user_master`
  ADD PRIMARY KEY (`um_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `item_master`
--
ALTER TABLE `item_master`
  MODIFY `im_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT for table `menu_master`
--
ALTER TABLE `menu_master`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `order_master`
--
ALTER TABLE `order_master`
  MODIFY `om_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `restaurant_detail`
--
ALTER TABLE `restaurant_detail`
  MODIFY `rd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `role_master`
--
ALTER TABLE `role_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `table_master`
--
ALTER TABLE `table_master`
  MODIFY `tm_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user_master`
--
ALTER TABLE `user_master`
  MODIFY `um_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
