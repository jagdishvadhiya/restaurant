$( document ).ready(function() {

	/* Act on the event */
	$("#emailerror").hide();
	
	$("#login").click(function(event) {
		/* Act on the event */
		event.preventDefault();
		$( ".errors" ).hide();
		checkEmail();
		// checkEmail function
		function checkEmail()
		{
			$("#emailerror").hide();
			var email = $( "#email" ).val();
			$.ajax({
				url: base_url + 'admin/ajax/emailCheck',
				type: 'POST',
				data: {email: email},
				success:the_response,
			});
			function the_response( response ){
				if( response == "1" )
					{
						loginNow();

					}else{
						$( "#emailerror" ).show();
						$( "#emailerror" ).val( "Email Not Found" );
						return false;
					}
			}
		}
		function loginNow ()
		{

			var email = $( "#email" ).val();
			var password = $( "#password" ).val();
			$.ajax({
				url: base_url + 'admin/login/login',
				type: 'POST',
				data: {
						email    : email,
						password : password
					  },
				success:function(responses){
					if( responses != "1" )
					{
						$( "#passerror" ).show();
					}else{
						 window.location.href = base_url + 'admin/dashboard';
					} 
				}
			});
			
			
		}
		
	});
});
