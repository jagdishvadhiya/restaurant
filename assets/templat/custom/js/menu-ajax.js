


$(document).ready(function(){  
  
   //set date piker
      $( "#datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
      });
    // form helper to check edit or add new form
     if($('#menuId' ).val())
    {
        var hasId = 'Updated';
        var r_id = $( "#menuRestaurantId" ).val();
        setRestautant( r_id, "selectRestaurant" );
      }else{
        var hasId = 'Added';
              setRestautant( "" , "selectRestaurant");
      }
    // for export the data in excel, csv, txt
     $( document ).on('click','#dropdownMenuButton',function(){
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var date = d.getFullYear() + '-' +
            ((''+month).length<2 ? '0' : '') + month + '-' +
            ((''+day).length<2 ? '0' : '') + day;
        var fileName = 'Menus '+date;
        $("table#menutable").tableExport({
              headings: false,                    // (Boolean), display table headings (th/td elements) in the <thead>
              footers: true,                     // (Boolean), display table footers (th/td elements) in the <tfoot>
              formats: ["xls","txt","csv"],    // (String[]), filetypes for the export
              fileName: fileName,                    // (id, String), filename for the downloaded file
              bootstrap: true,                   // (Boolean), style buttons using bootstrap
              position: "top" ,                // (top, bottom), position of the caption element relative to table
              ignoreRows: null,                  // (Number, Number[]), row indices to exclude from the exported file
              ignoreCols: [0,5],                 // (Number, Number[]), column indices to exclude from the exported file
              ignoreCSS: ".tableexport-ignore"   // (selector, selector[]), selector(s) to exclude from the exported file
            });
        var $buttons = $("table#menutable").find('caption').children().detach();
      $buttons.appendTo('#someOtherDiv');
      $('#someOtherDiv').find('button').after( "<br>" );
      $("table#menutable").find('caption').remove();
    });
     // pdf ecport
     $( document ).on('click','#pdfExport',function(){
     
               // $('table#menutable').tableExport({type:'pdf',escape:'false'});
                html2canvas($('table#menutable')[0], {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("cutomer-details.pdf");
                }
            });
            
     });


      
     // helper function for restaurant dropdown
     function setRestautant( r_id = "", id_var = "userRestaurant", ajax_url = "getRestaurantNameId" )
     {
       $.ajax({
              url: base_url + 'admin/ajax/'+ajax_url,
              type: 'POST',
              dataType: "json",
              success:function(response){
                if( response.status == true )
                {
                  $.each(response.data, function(key, value){
                    if( value.rd_id != r_id )
                    {
                      $("#"+id_var).append(new Option(value.rd_restaurant_name, value.rd_id));
                    }else{
                      $("#"+id_var).append(new Option(value.rd_restaurant_name, value.rd_id,false,true));
                    }
                    });
                }
              }
        });
      }

           // filter form processing
    $( "#filterForm" ).submit(function(e){
      e.preventDefault();
        var restaurantSelect = $( "#selectRestaurant" ).val();
        var selectdate = $( "#datepicker" ).val();
       
        if( restaurantSelect == "" && selectdate == "" )
        {
          alert( 'Please Select Any One Filter' );
         dataTable.ajax.reload();
        }else{
           dataTable.ajax.reload();
        }
    });
      // reset filter form
     $( '#clearFilter' ).click(function(){
      $('#datepicker').val('');
      $( "#selectRestaurant" ).val('');
          dataTable.ajax.reload();
        });

     
        var dataTable =   $('#menutable').DataTable({
                             "processing":true,  
                                 "serverSide":true,  
                                 "order":[],  
                                 "ajax":{  
                                      type:"POST",  
                                      url: base_url + 'admin/ajax/fetch_menu',
                                       data: function(data){
                                                  // Read values
                                                  var date = $('#datepicker').val();
                                                  var restaurant = $( "#selectRestaurant" ).val();
                                                  // Append to data
                                                  data.filter_date = date;
                                                  data.filter_restaurant = restaurant;
                                               }   
                                 },  
                                 "columnDefs":[  
                                    
                                    { "targets"        : 5,          "orderable":false}, 
                                    { "width"          : "10%",      "targets": 5 } 
                                 ],  
                           });  

      

      // form submit
    $( "#menuForm" ).submit(function(e){
        e.preventDefault();
        var form = $(this);
          //ajax validate and submit form
          $.ajax({
            url:form.attr('action'),
            type:'post',
            data:form.serialize(),
            dataType: "json",
            success:function(response){
               console.log( response );
               if( response.success == true )
                {
                  if( response.message.error == true )
                  {
                    swal("Menu "+ hasId +" Successfully!", {
                        icon: "success",
                      })
                    .then((willDelete) => {
                      if (willDelete) {
                        location.reload();
                      } 
                    });
                  }else{
                    swal("Menu "+ hasId +" Error!", {
                        icon: "error",
                      });
                  }
                  //remove error massages
                  $( '.text-danger' ).remove();
                  $( '.form-group' ).removeClass( 'has-error' )
                                    .removeClass( 'has-success' );
                }else{
                  $.each(response.message, function(key, value){
                    var element = $('#'+key);
                    element.closest('div.form-group')
                    .removeClass( 'has-error' )
                    .addClass( value.length > 0 ? 'has-error' : 'has-success')
                    .find( '.text-danger' )
                    .remove();
                    element.after(value);
                  });
                }
               
            }
        });
     });

     // delete user procesing
      $( document ).on('click','.delete',function() {
        var id = $(this).attr('id');
          swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this User Data!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    deleteMenu( id );
                    
                  } else {
                    swal("Menu is safe!");
                  }
                });
      });
       //delete user helper function
      function deleteMenu( id ){
          $.ajax({
            url: base_url + 'admin/menu/menu/deleteMenu',
            type: 'POST',
            data: { menu_id: id },
            success:function(response){
              if( response == 1 )
              {
                var dataTable =   $('#menutable').DataTable();
                dataTable.ajax.reload();
                swal("Menu has been deleted!", {
                      icon: "success",
                    });
              }else{
                swal("Menu is safe!");
              }
            }
          });   
      }

       // edit MEnu forword
      $( document ).on('click','.edit',function() {
        var id = $(this).attr('id');
        window.location = base_url + 'admin/menu/menu/menuAction/'+id; 
      });



      // show menu details on click show icon
         $( document ).on('click','.show',function() {
        var id = $(this).attr( 'id' );
        // document.location.href = base_url + 'admin/show/show';
          $.ajax({
            url: base_url + 'admin/menu/menu/getMenuById',
            type: 'POST',
            data: { menu_id: id },
            success:function(response){
              var data = JSON.parse( response );
              console.log( data );
              $( '#show_menu_name' ).html( data.menu_name );
              $( '#show_menu_price' ).html( data.menu_price );
              $( '#show_menu_r_name' ).html( data.restaurant_name );
              $( '#show_menu_date' ).html( data.menu_inserted );
             
            }
          }); 
      });

 });