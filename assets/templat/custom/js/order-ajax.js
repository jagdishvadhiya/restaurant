$(document).ready(function(){ 


  // for export the data in excel, csv, txt
 $( document ).on('click','#dropdownMenuButton',function(){
  var d = new Date();
  var month = d.getMonth()+1;
  var day = d.getDate();
  var date = d.getFullYear() + '-' +
      ((''+month).length<2 ? '0' : '') + month + '-' +
      ((''+day).length<2 ? '0' : '') + day;
  var fileName = 'Orders '+date;
  $("table#ordertable").tableExport({
        headings: false,                    // (Boolean), display table headings (th/td elements) in the <thead>
        footers: true,                     // (Boolean), display table footers (th/td elements) in the <tfoot>
        formats: ["xls","txt","csv"],    // (String[]), filetypes for the export
        fileName: fileName,                    // (id, String), filename for the downloaded file
        bootstrap: true,                   // (Boolean), style buttons using bootstrap
        position: "top" ,                // (top, bottom), position of the caption element relative to table
        ignoreRows: null,                  // (Number, Number[]), row indices to exclude from the exported file
        ignoreCols: [0,11],                 // (Number, Number[]), column indices to exclude from the exported file
        ignoreCSS: ".tableexport-ignore"   // (selector, selector[]), selector(s) to exclude from the exported file
      });
  var $buttons = $("table#ordertable").find('caption').children().detach();
// Append the buttons to an element of your choosing
$buttons.appendTo('#someOtherDiv');
// $('#someOtherDiv').find('button').append("<br>");
$('#someOtherDiv').find('button').after( "<br>" );
$("table#ordertable").find('caption').remove();
        

});
      
     //set date piker
      $( "#datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
      });
  if( $( "#orderId" ).val() )
  {
    var orderId = $( "#orderId" ).val();
    var orderRestaurantId = $( "#orderRestaurantId" ).val();
    var orderUserId = $( "#orderUserId" ).val();
    var orderTableId = $( "#orderTableId" ).val();
    var orderStatus = $("#orderStatus").val();
    if( orderStatus == "running" )
    {
      $("#selectStatus option[value='running']").attr("selected","selected");
    }else{
      $("#selectStatus option[value='finish']").attr("selected","selected");

    }
     $("#selectRestaurant option").remove();
    setRestautant( orderRestaurantId );
    setUser( orderUserId , "selectUser" , "getUserBYRestaurantId" , orderRestaurantId );
    setTable( orderTableId, "selectTable", "getTableBYRestaurantId" ,orderRestaurantId );
    setItemsTable( orderId );
    setMenu( r_id = "", id_var = "selectItemMenu", ajax_url = "getMenuNameId", orderRestaurantId  );
  }else{
     // $("#selectRestaurant option").remove();
     setRestautant( "" , "selectRestaurant");
     // setMenu( r_id = "", id_var = "selectItemMenu", ajax_url = "getMenuNameId", ""  );
  }

  function setItemsTable( order_id = "" )
  {
    $.ajax({
        url: base_url + 'admin/item/item/getItemsByOrderId/'+order_id,
        type: 'post',
        dataType: "json",
        success: function (data) {
         // console.log(data);
         // console.log(data[0].im_id);
         $.each(data, function(key,value){

          // console.log(value['im_id']);
          maketable( value['im_name'], value['im_amout'], value['im_qty'], value['im_total_amout'], value['im_item_id'] );
         });
         $("#itemdetails").show();
         $("#tfooter").show();

        }
      });
  }

      // filter form processing
    $( "#filterForm" ).submit(function(e){
      e.preventDefault();
        var restaurantSelect = $( "#selectRestaurant" ).val();
        var selectStatus = $( "#selectStatus" ).val();
        var selectdate = $( "#datepicker" ).val();
        if( restaurantSelect == "" && selectStatus == "" && selectdate == "" )
        {
          alert( 'Please Select Any One Filter' );
         dataTable.ajax.reload();
        }else{
          
          dataTable.ajax.reload();
        }
    });
    // reset filter form
     $( '#clearFilter' ).click(function(){
      $('#datepicker').val('');
      $( "#selectRestaurant" ).val('');
      $( "#selectStatus" ).val('');
          dataTable.ajax.reload();
        });
      // show datatable 
   
         var dataTable =   $('#ordertable').DataTable({
       "processing":true,  
           "serverSide":true,  
           "order":[],  
           "ajax":{  
                    type:"POST",  
                    url: base_url + 'admin/ajax/fetch_order',
                    data: function(data){
                              // Read values
                              var date = $('#datepicker').val();
                              var restaurant = $( "#selectRestaurant" ).val();
                              var status = $( "#selectStatus" ).val();
                              // Append to data
                              data.filter_date = date;
                              data.filter_restaurant = restaurant;
                              data.filter_status = status;
                    } 
                  },
           "columnDefs":[  
                          
                          { "targets"            : 11,          "orderable":false}, 
                          { "width"              : "50%",      "targets": 11 } 
                       ],  
     });  
     

   // var orderRestaurantId = $( "#orderRestaurantId" ).val()
   //    setMenu( r_id = "", id_var = "selectItemMenu", ajax_url = "getMenuNameId", orderRestaurantId  );
   
   // helper function for restaurant dropdown
   function setRestautant( r_id = "", id_var = "selectRestaurant", ajax_url = "getRestaurantNameId" )
   {

     $.ajax({
            url: base_url + 'admin/ajax/'+ajax_url,
            type: 'POST',
            dataType: "json",
            success:function(response){
              if( response.status == true )
              {
                $.each(response.data, function(key, value){
                  if( value.rd_id != r_id )
                  {
                    $("#"+id_var).append(new Option(value.rd_restaurant_name, value.rd_id));
                  }else{
                    $("#"+id_var).append(new Option(value.rd_restaurant_name, value.rd_id,false,true));
                  }
                  });
              }
            }
      });

    }
      // helper function for restaurant dropdown
   function setUser( r_id = "", id_var = "selectUser", ajax_url = "getUserBYRestaurantId" ,restaurant_id = "")
   {
     $.ajax({
            url: base_url + 'admin/ajax/'+ajax_url+"/"+restaurant_id,
            type: 'POST',
            dataType: "json",
            success:function(response){
              if( response.status == true )
              {
                $("#selectUser option").remove();
                     // $("#selectUser").append(new Option("Select User", ""));
                $.each(response.data, function(key, value){
                  if( value.um_id != r_id )
                  {
                    $("#"+id_var).append(new Option(value.um_name, value.um_id));
                  }else{
                    $("#"+id_var).append(new Option(value.um_name, value.um_id,false,true));
                  }
                  });
              }
            }
      });
    }

       // helper function for Table dropdown
   function setTable( r_id = "", id_var = "selectTable", ajax_url = "getTableBYRestaurantId" ,restaurant_id = "")
   {
     $.ajax({
            url: base_url + 'admin/ajax/'+ajax_url+"/"+restaurant_id,
            type: 'POST',
            dataType: "json",
            success:function(response){
              if( response.status == true )
              {
                $("#selectTable option").remove();
                     // $("#selectTable").append(new Option("Select User", ""));
                $.each(response.data, function(key, value){
                  if( value.tm_id != r_id )
                  {
                    $("#"+id_var).append(new Option(value.tm_name, value.tm_id));
                  }else{
                    $("#"+id_var).append(new Option(value.tm_name, value.tm_id,false,true));
                  }
                  });
              }
            }
      });
    }
    // set menu dynamically

    // setMenu();
     // setMenu();
    function setMenu( r_id = "", id_var = "selectItemMenu", ajax_url = "getMenuNameId", restaurant_id = "" )
     {
      
       $.ajax({
              url: base_url + 'admin/ajax/'+ajax_url+'/'+restaurant_id,
              type: 'POST',
              dataType: "json",
              success:function(response){
               
                if( response.status == true )
                {
            
                  $("#selectItemMenu option").remove();
                  $.each(response.data, function(key, value){
                    if( value.menu_id != r_id )
                    {
                      $("#"+id_var).append(new Option(value.menu_name, value.menu_id));
                    }else{
                      $("#"+id_var).append(new Option(value.menu_name, value.menu_id,false,true));
                    }
                    });
                  setMenuPrice();
                }
              }
        });
       

      }
      function setMenuPrice(menu_id = "")
     {

      var menu_id = $( "#selectItemMenu option:selected" ).val();
       $.ajax({
              url: base_url + 'admin/ajax/getMenuPriceBYId/'+menu_id,
              type: 'POST',
              dataType: "json",
              success:function(response){
               $( '#itemPrice' ).val( response );
               setTotalAmout();
              }
        });
      }
       $("#itemdetails").hide();
         $("#tfooter").hide();
      // add new item button click action
      $( "#AddNewItem" ).click(function (e) {
         e.preventDefault();
         if( $( "#selectItemMenu option:selected" ).val() != "" )
         {
          var itemId = $( "#selectItemMenu option:selected" ).val();
              var itemName = $( "#selectItemMenu option:selected" ).text();
         var itemPrice = $( "#itemPrice" ).val();
         var itemQty = $( "#orderQty" ).val();
         var itemTotal = $( "#totalAmout" ).val();
         $("#itemdetails").show();
         $("#tfooter").show();
        
         maketable(itemName , itemPrice, itemQty, itemTotal, itemId);
         // setTotal();
         }else{
          swal("Please Select Item!", {
                        icon: "error",
                      });
         }
       
          
       });

     
       // make table 
       function maketable(item = "PIZA", price = "100", qty = "2", total = "200", itemId = ''){
        
        var row = "<tr><td>"+item+"</td><td>" + price + "</td><td>" + qty + "</td><td id='totalprice'>" + total + "</td><td id='"+itemId+"'><button type='button' class='btn btn-danger' id='removeItem'>X</button></td></tr>";
        $( "#itemdetailsbody" ).append(row);
        totalCount();
        // getOrderItems();
        
       } 
    // table remove column
    $(document).on('click', "#removeItem" ,function (e) {
      e.preventDefault();
      
      $(this).closest("tr").remove();
      totalCount();
    });
    function totalCount(){
      var sum = 0;
       $('tr').each(function() {
                $(this).find('#totalprice').each(function(i){        
                    sum += parseInt($(this).html());
                });
            });
       if(sum == 0){
          $("#itemdetails").hide();
         $("#tfooter").hide();
       }
       $('#totalPriceAmout').html(sum);
      
    }
    // set users on restaurant select
    $( "#selectRestaurant" ).change(function (e) {
      $("#selectItemMenu option").remove();
       $("#selectItemMenu").append(new Option("Select Menu", ""));
      var restaurant_id = $("#selectRestaurant").val();
      var user_select_id = $("#selectUser").val();
      if( restaurant_id != '' )
      {

        setUser(r_id = "", id_var = "selectUser", ajax_url = "getUserBYRestaurantId" ,restaurant_id);
        setTable( r_id = "", id_var = "selectTable", ajax_url = "getTableBYRestaurantId" ,restaurant_id );
        setMenu( r_id = "", id_var = "selectItemMenu", ajax_url = "getMenuNameId", restaurant_id  );

      }

        //    var menu_id = $("#selectItemMenu").val();
        // setMenuPrice( menu_id );                   

        // // setTotalAmout();

    });
     // set Menu price on Menu select
    $( document ).change( "#selectItemMenu",function (e) {
      e.preventDefault();
      var select_menu = $( "#selectItemMenu" ).val();
      if( select_menu == "" )
      {
        $( '#itemPrice' ).val( '0' );
      }else{
         var menu_id = $("#selectItemMenu").val();
     setMenuPrice( menu_id );
     
      }
     

    });
     // calculate quentity and price and show in text box
      $( "#itemPrice" ).change(function() {
     setTotalAmout();
    });
      $( "#orderQty" ).change(function() {
     setTotalAmout();
    });
     
      function setTotalAmout(){
        var price = parseInt( $( "#itemPrice" ).val() );
        var qty = parseInt( $( "#orderQty" ).val() );
        var total = price * qty ;
        $( "#totalAmout" ).val( total );
      }

      function getOrderItems(){
        var TableData = new Array();
    
            $('#itemdetails tr').each(function(row, tr){
              TableData[row]={
                  "itemName" : $(tr).find('td:eq(0)').text()
                  , "itemPrice" :$(tr).find('td:eq(1)').text()
                  , "itemQty" : $(tr).find('td:eq(2)').text()
                  , "itemTotalPrice" : $(tr).find('td:eq(3)').text()
                  , "itemItemId" : $(tr).find('td:eq(4)').attr('id')
              }
            }); 
            TableData.shift();
            TableData.pop();
            return TableData;

      }
  // $("#AddNewItem").click(function(){
  //       $(".itemDiv").clone().appendTo(".cardDiv");
  //     });
      // form submit
    $( "#orderForm" ).submit(function(e){
        e.preventDefault();
        
        var form = $(this);
        var error = false;
        var itemArray = getOrderItems();
        var cname = $("#customerName").val();
        var cmnumber = $("#customerNumber").val();
        var restaurantSelect = $("#selectRestaurant").val();
        var userSelect = $("#selectUser").val();
        var userName = $("#selectUser option:selected").html();
        var tableName = $("#selectTable option:selected").html();
        var tableId = $("#selectTable").val();
        var status = $( "#selectStatus" ).val();
        // var totalItemsAmout = sum;
        if(cname == "" || cmnumber == "" || restaurantSelect == "" || userSelect == "" || tableId == "" || status == "" )
        {
          swal("Please Enter All Details !", {
                        icon: "error",
                      });
          error = true;
        }else{
          error = false;
        }
        if(itemArray == "")
        {
          swal("Please Select Any One Item!", {
                        icon: "error",
                      });
          error = true;
        }
        if(!error){
            
            // itemArray['name'] = 'jk';
            // itemArray['data']='k';
        
             var ostatus = $( "#selectStatus" ).val();
            if( $( "#orderId" ).val() )
             {
               
               var orderId = $( "#orderId" ).val();
               var hasId = "Update";
             }else{
               var hasId = "Added";

             }
            // console.log(d);
              
              // ajax validate and submit form
              $.ajax({
                url:form.attr('action'),
                type:'POST',
                data: { 
                        cname: cname,
                        cmnumber: cmnumber,
                        restaurantSelect: restaurantSelect,
                        userSelect: userSelect,
                        ostatus: ostatus,
                        userName: userName,
                        tableName:tableName,
                        tableId:tableId,
                        orderId:orderId,
                        itemData : itemArray
                      },
                // data: f,
                
                dataType: "json",
                success:function(response){
                  // console.log( response );
                  // console.log( response.success );
                   if( response.success == true )
                   {
                    swal("Order "+ hasId +" Successfully!", {
                        icon: "success",

                      })
                       .then((willDelete) => {
                          if (willDelete) {
                            location.reload();
                          }
                        });
                   }else{
                    swal("Order "+ hasId +" Failed!", {
                        icon: "error",
                      });
                   }
                 }
                  
            });
        }
     });
   
    // edit order forword
      $( document ).on('click','.edit',function() {
        var id = $(this).attr('id');
        window.location = base_url + 'admin/order/order/orderAction/'+id; 
      });

       // delete ORDER procesing
      $( document ).on('click','.delete',function() {
        var id = $(this).attr('id');
          swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this Order Data!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    deleteOrder( id );
                    
                  } else {
                    swal("Order is safe!");
                  }
                });
      });
       //delete Order helper function
      function deleteOrder( id ){
          $.ajax({
            url: base_url + 'admin/order/order/deleteOrder',
            type: 'POST',
            data: { order_id: id },
            success:function(response){
              if( response == 1 )
              {
                 var dataTable =   $('#ordertable').DataTable();
                dataTable.ajax.reload();
                swal("Order has been deleted!", {
                      icon: "success",
                    });
              }else{
                swal("Order is safe!");
              }
            }
          });   
      }



      // show menu details on click show icon
         $( document ).on('click','.show',function() {
          var id = $(this).attr( 'id' );
          // document.location.href = base_url + 'admin/show/show';
          $.ajax({
            url: base_url + 'admin/order/order/getOrderById',
            type: 'POST',
            data: { order_id: id },
            success:function(response){
              var data = JSON.parse( response );
              // console.log( data );
              $( '#show_c_name' ).html( data.om_customer_name );
              $( '#show_c_mobile' ).html( data.om_mobile );
              $( '#show_r_name' ).html( data.restaurant_name );
              $( '#show_o_items' ).html( data.om_total_item );
              $( '#show_o_amout' ).html( data.om_total_amount );
              $( '#show_o_status' ).html( data.om_status );
              $( '#show_o_user' ).html( data.om_user_name );
              $( '#show_o_date' ).html( data.om_inserted_date );
              $.each(data['items'], function(key,value){
                var row = '<tr> <td>'+value['im_name']+'</td> <td>'+value['im_amout']+'</td> <td>'+value['im_qty']+'</td> <td>'+value['im_total_amout']+' Rs.</td> </tr>';
                $( '#itemTab' ).append( row );
              });
             
            }
          }); 
      });
    
 });