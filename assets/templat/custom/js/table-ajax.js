
$(document).ready(function(){  
  // for export the data in excel, csv, txt
     $( document ).on('click','#dropdownMenuButton',function(){
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var date = d.getFullYear() + '-' +
            ((''+month).length<2 ? '0' : '') + month + '-' +
            ((''+day).length<2 ? '0' : '') + day;
        var fileName = 'Tables '+date;
        $("table#Tabletable").tableExport({
              headings: false,                    // (Boolean), display table headings (th/td elements) in the <thead>
              footers: true,                     // (Boolean), display table footers (th/td elements) in the <tfoot>
              formats: ["xls","txt","csv"],    // (String[]), filetypes for the export
              fileName: fileName,                    // (id, String), filename for the downloaded file
              bootstrap: true,                   // (Boolean), style buttons using bootstrap
              position: "top" ,                // (top, bottom), position of the caption element relative to table
              ignoreRows: null,                  // (Number, Number[]), row indices to exclude from the exported file
              ignoreCols: [0,4],                 // (Number, Number[]), column indices to exclude from the exported file
              ignoreCSS: ".tableexport-ignore"   // (selector, selector[]), selector(s) to exclude from the exported file
            });
        var $buttons = $("table#Tabletable").find('caption').children().detach();
      $buttons.appendTo('#someOtherDiv');
      $('#someOtherDiv').find('button').after( "<br>" );
      $("table#Tabletable").find('caption').remove();
    });

     //set date piker
      $( "#datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
      });
    // form helper to check edit or add new form
     if($('#tableId' ).val())
    {
        var hasId = 'Updated';
        var r_id = $( "#tableRestaurantId" ).val();
        setRestautant( r_id, "selectRestaurant" );
      }else{
        var hasId = 'Added';
              setRestautant( "" , "selectRestaurant");
      }

      // helper function for restaurant dropdown
     function setRestautant( r_id = "", id_var = "selectRestaurant", ajax_url = "getRestaurantNameId" )
     {
       $.ajax({
              url: base_url + 'admin/ajax/'+ajax_url,
              type: 'POST',
              dataType: "json",
              success:function(response){
                if( response.status == true )
                {
                  $.each(response.data, function(key, value){
                    if( value.rd_id != r_id )
                    {
                      $("#"+id_var).append(new Option(value.rd_restaurant_name, value.rd_id));
                    }else{
                      $("#"+id_var).append(new Option(value.rd_restaurant_name, value.rd_id,false,true));
                    }
                    });
                }
              }
        });
      }

            // filter form processing
    $( "#filterForm" ).submit(function(e){
      e.preventDefault();
        var restaurantSelect = $( "#selectRestaurant" ).val();
        var selectdate = $( "#datepicker" ).val();
       
        if( restaurantSelect == "" && selectdate == "" )
        {
          alert( 'Please Select Any One Filter' );
         dataTable.ajax.reload();
        }else{
          
         dataTable.ajax.reload();
        }
    });
     // reset filter form
     $( '#clearFilter' ).click(function(){
      $('#datepicker').val('');
      $( "#selectRestaurant" ).val('');
          dataTable.ajax.reload();
        });
      
      // datatable show function
   
          var dataTable =   $('#Tabletable').DataTable({
                                "processing":true,  
                                   "serverSide":true,  
                                   "order":[],  
                                   "ajax":{  
                                        type:"POST",  
                                        url: base_url + 'admin/ajax/fetch_table',
                                        data: function(data){
                                                // Read values
                                                var date = $('#datepicker').val();
                                                var restaurant = $( "#selectRestaurant" ).val();
                                                // Append to data
                                                data.filter_date = date;
                                                data.filter_restaurant = restaurant;
                                             }
                              
                                   },  
                                   "columnDefs":[  
                                      
                                      { "targets"        : 4,          "orderable":false}, 
                                      { "width"          : "10%",      "targets": 4 } 
                                   ],  
                              }); 
       
       



      // form submit
    $( "#tableForm" ).submit(function(e){
        e.preventDefault();
        var form = $(this);
          //ajax validate and submit form
          $.ajax({
            url:form.attr('action'),
            type:'post',
            data:form.serialize(),
            dataType: "json",
            success:function(response){
               console.log( response );
               if( response.success == true )
                {
                  if( response.message.error == true )
                  {
                    swal("Table "+ hasId +" Successfully!", {
                        icon: "success",
                      })
                    .then((willDelete) => {
                      if (willDelete) {
                        location.reload();
                      } 
                    });
                  }else{
                    swal("Table "+ hasId +" Error!", {
                        icon: "error",
                      });
                  }
                  //remove error massages
                  $( '.text-danger' ).remove();
                  $( '.form-group' ).removeClass( 'has-error' )
                                    .removeClass( 'has-success' );
                }else{
                  $.each(response.message, function(key, value){
                    var element = $('#'+key);
                    element.closest('div.form-group')
                    .removeClass( 'has-error' )
                    .addClass( value.length > 0 ? 'has-error' : 'has-success')
                    .find( '.text-danger' )
                    .remove();
                    element.after(value);
                  });
                }
               
            }
        });
     });

    // delete user procesing
      $( document ).on('click','.delete',function() {
        var id = $(this).attr('id');
          swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this User Data!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    deleteTable( id );
                    
                  } else {
                    swal("Table is safe!");
                  }
                });
      });
       //delete user helper function
      function deleteTable( id ){
          $.ajax({
            url: base_url + 'admin/table/table/deleteTable',
            type: 'POST',
            data: { table_id: id },
            success:function(response){
              if( response == 1 )
              {
                var dataTable =   $('#Tabletable').DataTable();
                dataTable.ajax.reload();
                swal("Table has been deleted!", {
                      icon: "success",
                    });
              }else{
                swal("Table is safe!");
              }
            }
          });   
      }

     // edit Table forword
      $( document ).on('click','.edit',function() {
        var id = $(this).attr('id');
        window.location = base_url + 'admin/table/table/tableAction/'+id; 
      });

       // show table details on click show icon
         $( document ).on('click','.show',function() {
        var id = $(this).attr( 'id' );
        // document.location.href = base_url + 'admin/show/show';
          $.ajax({
            url: base_url + 'admin/table/table/getTableById',
            type: 'POST',
            data: { table_id: id },
            success:function(response){
              var data = JSON.parse( response );
              console.log( data );
              $( '#show_table_name' ).html( data.tm_name );
              $( '#show_table_r_name' ).html( data.restaurant_name );
              $( '#show_table_date' ).html( data.inserted_date );
             
            }
          }); 
      });
 });
