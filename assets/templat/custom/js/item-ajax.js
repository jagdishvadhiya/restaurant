$(document).ready(function(){



 $( document ).on('click','#dropdownMenuButton',function(){
  $("table#itemtable").tableExport({
        headings: false,                    // (Boolean), display table headings (th/td elements) in the <thead>
        footers: true,                     // (Boolean), display table footers (th/td elements) in the <tfoot>
        formats: ["xls","txt","csv"],    // (String[]), filetypes for the export
        fileName: "Items",                    // (id, String), filename for the downloaded file
        bootstrap: true,                   // (Boolean), style buttons using bootstrap
        position: "top" ,                // (top, bottom), position of the caption element relative to table
        ignoreRows: null,                  // (Number, Number[]), row indices to exclude from the exported file
        ignoreCols: [0,8],                 // (Number, Number[]), column indices to exclude from the exported file
        ignoreCSS: ".tableexport-ignore"   // (selector, selector[]), selector(s) to exclude from the exported file
      });
  var $buttons = $("table#itemtable").find('caption').children().detach();
// Append the buttons to an element of your choosing
$buttons.appendTo('#someOtherDiv');
// $('#someOtherDiv').find('button').append("<br>");
$('#someOtherDiv').find('button').after( "<br>" );
$("table#itemtable").find('caption').remove();
        

});

   //set date piker
      $( "#datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
      });
      //form submit helper variable
  if($('#itemId' ).val())
    {
        var hasId = 'Updated';
        var menuId = $( "#itemMenuId" ).val();
          
      }else{
        var hasId = 'Added';
               
      }  
  // show user name
  if( $( "#itemOrderId" ).val() )
  {
    var order_id = $( "#itemOrderId" ).val();
    $.ajax({
              url: base_url + 'admin/ajax/setUserNameByOrderIdAjax/'+order_id,
              type: 'POST',
              
              success:function(response){
                $( "#itemOrderUserName" ).val( response );
               }
             });
  }

  // calculate quentity and price and show in text box
      $( "#itemPrice" ).change(function() {
     setTotalAmout();
    });
      $( "#itemQty" ).change(function() {
     setTotalAmout();
    });
      function setTotalAmout(){
        var price = parseInt( $( "#itemPrice" ).val() );
        var qty = parseInt( $( "#itemQty" ).val() );
        var total = price * qty ;
        $( "#totalAmout" ).val( total );
      }

      
      var dataTable =   $('#itemtable').DataTable({
                                 "processing":true,  
                                 "serverSide":true,  
                                 "order":[],  
                                     "ajax":{  
                                                  type:"POST",  
                                                  url: base_url + 'admin/ajax/fetch_item' , 
                                                  data: function(data){
                                                            // Read values
                                                            var date = $('#datepicker').val();
                                                            var itemOrderId = $(".itemOrderId").val();
                                                            // Append to data
                                                            data.filter_date = date;
                                                            data.filter_itemOrderId = itemOrderId;
                                                         }
                                             },  
                                 "columnDefs":[  
                                                   { "targets"           : 8,          "orderable":false}, 
                                                   { "width"             : "10%",      "targets": 8 } 
                                               ],
      //                             "dom": 'Bfrtip',
      // //                             dom: `<'row'<'col-sm-6 text-left'f><'col-sm-6 text-right'B>>
      // // <'row'<'col-sm-12'tr>>
      // // <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
      //                             "buttons": [
      //                                           'pdfHtml5',
                                                
      //                                       ]
                               
                                
                        }); 
    


         $( "#filterForm" ).submit(function(e){
          e.preventDefault();
            dataTable.ajax.reload();
        }); 

    $( '#clearFilter' ).click(function(){
      $('#datepicker').val('');
          dataTable.ajax.reload();
        });
  
      // setMenu();
      // helper function for restaurant dropdown
     function setMenu( r_id = "", id_var = "selectItemMenu", ajax_url = "getMenuNameId" )
     {
      
       $.ajax({
              url: base_url + 'admin/ajax/'+ajax_url,
              type: 'POST',
              dataType: "json",
              success:function(response){
               
                if( response.status == true )
                {
                  $.each(response.data, function(key, value){
                    if( value.menu_id != r_id )
                    {
                      $("#"+id_var).append(new Option(value.menu_name, value.menu_id));
                    }else{
                      $("#"+id_var).append(new Option(value.menu_name, value.menu_id,false,true));
                    }
                    });
                }
              }
        });
      }

         // form submit
    $( "#itemForm" ).submit(function(e){
        e.preventDefault();
        var form = $(this);
          //ajax validate and submit form
          $.ajax({
            url:form.attr('action'),
            type:'post',
            data:form.serialize(),
            dataType: "json",
            success:function(response){
               console.log( response );
               if( response.success == true )
                {
                  if( response.message.error == true )
                  {
                    swal("Menu "+ hasId +" Successfully!", {
                        icon: "success",
                      })
                    .then((willDelete) => {
                      if (willDelete) {
                        location.reload();
                      } 
                    });
                  }else{
                    swal("Menu "+ hasId +" Error!", {
                        icon: "error",
                      });
                  }
                  //remove error massages
                  $( '.text-danger' ).remove();
                  $( '.form-group' ).removeClass( 'has-error' )
                                    .removeClass( 'has-success' );
                }else{
                  $.each(response.message, function(key, value){
                    var element = $('#'+key);
                    element.closest('div.form-group')
                    .removeClass( 'has-error' )
                    .addClass( value.length > 0 ? 'has-error' : 'has-success')
                    .find( '.text-danger' )
                    .remove();
                    element.after(value);
                  });
                }
               
            }
        });
     });


       // edit Item forword
      $( document ).on('click','.edit',function() {
        var id = $(this).attr('id');
        window.location = base_url + 'admin/item/item/itemAction/'+id; 
      });


       // delete user procesing
      $( document ).on('click','.delete',function() {
        var id = $(this).attr('id');
          swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this User Data!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    deleteItem( id );
                    
                  } else {
                    swal("Item is safe!");
                  }
                });
      });
       //delete user helper function
      function deleteItem( id ){
          $.ajax({
            url: base_url + 'admin/item/item/deleteItem',
            type: 'POST',
            data: { item_id: id },
            success:function(response){
              if( response == '1' )
              {
                 var dataTable =   $('#itemtable').DataTable();
                dataTable.ajax.reload();
                  swal("Item has been deleted!", {
                      icon: "success",
                    });
              
              }else{
                swal("Item is safe!");
              }
            }
          });   
      }



      // show menu details on click show icon
         $( document ).on('click','.show',function() {
        var id = $(this).attr( 'id' );
        // document.location.href = base_url + 'admin/show/show';
          $.ajax({
            url: base_url + 'admin/item/item/getItemById',
            type: 'POST',
            data: { item_id: id },
            success:function(response){
              var data = JSON.parse( response );
              console.log( data );
              $( '#show_item_name' ).html( data.im_name );
              $( '#show_item_price' ).html( data.im_amout );
              $( '#show_item_qty' ).html( data.im_qty );
              $( '#show_item_total' ).html( data.im_total_amout );
              $( '#show_item_order' ).html( data.user_name );
              $( '#show_item_date' ).html( data.im_inserted_date );
             
            }
          }); 
      });
 });