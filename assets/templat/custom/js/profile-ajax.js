
$(document).ready(function(){ 
  // role management on checkbox click update roles
  $( document ).on('click', '.checkbox',function(){
            var user_type = $(this).data('utype');
                var user_id = $(this).val();
                var action_type = $(this).attr('id');
            if($(this).prop("checked") == true){
                
                var action = 1;
            }
            if($(this).prop("checked") == false){
                var action = 0;
            }
             // console.log("Checkbox is checked. "+ user_id + user_type + action_type + action);
              $.ajax({
                url:base_url + 'admin/genral/permitionAction',
                type:'post',
                data:{
                        'user_id' : user_id,
                        'action'  : action_type,
                        'value'   : action
                      },
                dataType: "json",
                success:function(response){
                  console.log( response );
                  if( response.success == true )
                  {

                     $( '#premitionMsg' ).show();
                      setInterval(function () {
                        $( '#premitionMsg' ).hide();
                      },5000);
                       console.log( "permition  Updated" );
                  }else{
                    console.log( "permition Not Updated" );

                  }
                }
            });
  });
  $( '#premitionMsg' ).hide();
  // profile update helper function
	function checkForm()
	{
		 var restaurantName = $( "#restaurantName" ).val();
      var ownerName = $( "#ownerName" ).val();
      var ownerEmail = $( "#ownerEmail" ).val();
      var ownerMobile = $( "#ownerMobile" ).val();
      var ownerAddress = $( "#ownerAddress" ).val();
      var password = $( "#password" ).val();
      var confirmPassword = $( "#confirmPassword" ).val();
      if( restaurantName == "" || ownerName == "" || ownerEmail == "" || ownerMobile == "" || ownerAddress == "" )
      {
      	alert( "Please Fill All Details" );
        return false;
      }
      if( password != "" )
      {
      	if( password !== confirmPassword  )
      	{
      		alert( "Please Enter Same Password and Confirm Password" );
            return false;
      	}
      } 
      
	} 
	// update profile
	 $( "#profileform" ).submit(function(e){
        e.preventDefault();
         if( checkForm() != false )
         {

         
          // alert('a');
           var form = $(this);
              //ajax validate and submit form
              $.ajax({
                url:form.attr('action'),
                type:'post',
                data:form.serialize(),
                dataType: "json",
                success:function(response){
                  if( response.success == true )
                  {
                    if( response.message.error == true )
                    {

                      swal("Restaurant updated Successfully!", {
                          icon: "success",
                        })
                      .then((willDelete) => {
                      if (willDelete) {
                        location.reload();
                        
                      } 
                    });

                    }else{
                      swal("Restaurant Update Error!", {
                          icon: "error",
                        });
                    }
                    //remove error massages
                    $( '.text-danger' ).remove();
                    $( '.form-group' ).removeClass( 'has-error' )
                                      .removeClass( 'has-success' );
                    form[0].reset();
                     
                     
                  }else{
                    $.each(response.message, function(key, value){
                      var element = $('#'+key);
                      element.closest('div.form-group')
                      .removeClass( 'has-error' )
                      .addClass( value.length > 0 ? 'has-error' : 'has-success')
                      .find( '.text-danger' )
                      .remove();
                      element.after(value);
                    });
                  }
                 }
      	       }); 
         }
      });

});