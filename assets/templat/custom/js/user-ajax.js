
$(document).ready(function(){  
      // for export the data in excel, csv, txt
     $( document ).on('click','#dropdownMenuButton',function(){
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var date = d.getFullYear() + '-' +
            ((''+month).length<2 ? '0' : '') + month + '-' +
            ((''+day).length<2 ? '0' : '') + day;
        var fileName = 'Tables '+date;
        $("table#usertable").tableExport({
              headings: false,                    // (Boolean), display table headings (th/td elements) in the <thead>
              footers: true,                     // (Boolean), display table footers (th/td elements) in the <tfoot>
              formats: ["xls","txt","csv"],    // (String[]), filetypes for the export
              fileName: fileName,                    // (id, String), filename for the downloaded file
              bootstrap: true,                   // (Boolean), style buttons using bootstrap
              position: "top" ,                // (top, bottom), position of the caption element relative to table
              ignoreRows: null,                  // (Number, Number[]), row indices to exclude from the exported file
              ignoreCols: [0,7],                 // (Number, Number[]), column indices to exclude from the exported file
              ignoreCSS: ".tableexport-ignore"   // (selector, selector[]), selector(s) to exclude from the exported file
            });
        var $buttons = $("table#usertable").find('caption').children().detach();
      $buttons.appendTo('#someOtherDiv');
      $('#someOtherDiv').find('button').after( "<br>" );
      $("table#usertable").find('caption').remove();
    });

  //check edit or add new action
  
    // load restaurant in drop down dynamix
    if($('#userId' ).val())
    {
        var hasId = 'Updated';
        var r_id = $( "#userRestaurantId" ).val();
        setRestautant( r_id );
        $( "#userRestaurant" ).closest( '.form-group' ).removeClass('d-none');
    }else{
        $( "#userRestaurant" ).closest( '.form-group' ).addClass('d-none');
           setRestautant();
         var hasId = 'Added';
    }
    setRestautant( "" , "selectRestaurant");
  
   // helper function for restaurant dropdown
   function setRestautant( r_id = "", id_var = "userRestaurant", ajax_url = "getRestaurantNameId" )
   {
     $.ajax({
            url: base_url + 'admin/ajax/'+ajax_url,
            type: 'POST',
            dataType: "json",
            success:function(response){
              if( response.status == true )
              {
                $.each(response.data, function(key, value){
                  if( value.rd_id != r_id )
                  {
                    $("#"+id_var).append(new Option(value.rd_restaurant_name, value.rd_id));
                  }else{
                    $("#"+id_var).append(new Option(value.rd_restaurant_name, value.rd_id,false,true));
                  }
                  });
              }
            }
      });
    }
    // $( " .delete" ).hide();

    // form submit
    $( "#userForm" ).submit(function(e){
        e.preventDefault();
        var form = $(this);
          //ajax validate and submit form
          $.ajax({
            url:form.attr('action'),
            type:'post',
            data:form.serialize(),
            dataType: "json",
            success:function(response){
               console.log( response );
               if( response.success == true )
                {
                  if( response.message.error == true )
                  {
                    swal("User "+ hasId +" Successfully!", {
                        icon: "success",
                      })
                    .then((willDelete) => {
                      if (willDelete) {
                        location.reload();
                      } 
                    });
                  }else{
                    swal("User "+ hasId +" Error!", {
                        icon: "error",
                      });
                  }
                  //remove error massages
                  $( '.text-danger' ).remove();
                  $( '.form-group' ).removeClass( 'has-error' )
                                    .removeClass( 'has-success' );
                  form[0].reset();
                }else{
                  $.each(response.message, function(key, value){
                    var element = $('#'+key);
                    element.closest('div.form-group')
                    .removeClass( 'has-error' )
                    .addClass( value.length > 0 ? 'has-error' : 'has-success')
                    .find( '.text-danger' )
                    .remove();
                    element.after(value);
                  });
                }
            }
        });
     });
    // dynamic show or hide restaurant dropdown
     $( document ).on("change","#userType",function() {
          var userType = $(this).val();
          if( userType != "" && userType != "superadmin" )
          {
            var selectRestaurant = $("#userRestaurant");
            selectRestaurant.closest('div.form-group')
            .removeClass( 'd-none' );
          }else{
            var selectRestaurant = $("#userRestaurant");
            selectRestaurant.closest('div.form-group')
            .addClass( 'd-none' );
          }
     });
       // filter form processing
    $( "#filterForm" ).submit(function(e){
      e.preventDefault();
         var user = $('#selectUser').val();
        var restaurant = $("#selectRestaurant").val();
        if( restaurant == "" &&  user == "" )
        {
          alert( 'Please Select Any One Filter' );
           dataTable.ajax.reload();
        }else{
          
            dataTable.ajax.reload();
        }
    });
    $( '#clearFilter' ).click(function(e){
       e.preventDefault();
          $('#selectUser').val('');
          $("#selectRestaurant").val('');
          dataTable.ajax.reload();
        });
    // show datatable in on page load
    
    
      var dataTable = $('#usertable').DataTable({
                        "processing":true,  
                           "serverSide":true,  
                           "order":[],  
                           "ajax":{  
                                type:"POST",  
                                url: base_url + 'admin/ajax/fetch_user',
                                data: function(data){
                                      // Read values
                                      var user = $('#selectUser').val();
                                      var restaurant = $("#selectRestaurant").val();
                                      // Append to data
                                      data.filter_restaurant = restaurant;
                                      data.filter_user = user;
                                   }
                           },  
                           "columnDefs":[  
                             
                              { "um_inserted_date": "engine",   "targets": 6 },
                                { "targets": 7, "orderable":false}, 
                                 { "width": "10%", "targets": 7 } 
                           ],  
                      }); 
    



      
      // delete user procesing
      $( document ).on('click','.delete',function() {
        var id = $(this).attr('id');
          swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this User Data!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    deleteUser( id );
                    
                  } else {
                    swal("User is safe!");
                  }
                });
      });
      
      //delete user helper function
      function deleteUser( id ){
          $.ajax({
            url: base_url + 'admin/user/user/deleteuser',
            type: 'POST',
            data: { user_id: id },
            success:function(response){
              if( response == 1 )
              {
                var dataTable = $('#usertable').DataTable();
                dataTable.ajax.reload();
                swal("User has been deleted!", {
                      icon: "success",
                    });
              }else{
                swal("User is safe!");
              }
            }
          });   
      }

      // edit user forword
      $( document ).on('click','.edit',function() {
        var id = $(this).attr('id');
        window.location = base_url + 'admin/user/user/userAction/'+id; 
      });

      // view user profile
      $( document ).on('click','.show',function() {
        var id = $(this).attr( 'id' );
        // document.location.href = base_url + 'admin/show/show';
          $.ajax({
            url: base_url + 'admin/user/user/getUserById',
            type: 'POST',
            data: { user_id: id },
            success:function(response){
              var data = JSON.parse( response );
              console.log( data );
              $( '#show_name' ).html( data.um_name );
              $( '#show_email' ).html( data.um_email );
              $( '#show_mobile' ).html( data.um_mobile );
              $( '#show_r_id' ).html( data.restaurant_name );
              $( '#show_type' ).html( data.um_type );
              $( '#show_date' ).html( data.um_inserted_date );
            }
          }); 
      });

 });

