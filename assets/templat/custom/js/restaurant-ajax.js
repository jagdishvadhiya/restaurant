
$(document).ready(function(){  
    // for export the data in excel, csv, txt
     $( document ).on('click','#dropdownMenuButton',function(){
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var date = d.getFullYear() + '-' +
            ((''+month).length<2 ? '0' : '') + month + '-' +
            ((''+day).length<2 ? '0' : '') + day;
        var fileName = 'Restaurants '+date;
        $("table#restauranttable").tableExport({
              headings: false,                    // (Boolean), display table headings (th/td elements) in the <thead>
              footers: true,                     // (Boolean), display table footers (th/td elements) in the <tfoot>
              formats: ["xls","txt","csv"],    // (String[]), filetypes for the export
              fileName: fileName,                    // (id, String), filename for the downloaded file
              bootstrap: true,                   // (Boolean), style buttons using bootstrap
              position: "top" ,                // (top, bottom), position of the caption element relative to table
              ignoreRows: null,                  // (Number, Number[]), row indices to exclude from the exported file
              ignoreCols: [0,7],                 // (Number, Number[]), column indices to exclude from the exported file
              ignoreCSS: ".tableexport-ignore"   // (selector, selector[]), selector(s) to exclude from the exported file
            });
        var $buttons = $("table#restauranttable").find('caption').children().detach();
      $buttons.appendTo('#someOtherDiv');
      $('#someOtherDiv').find('button').after( "<br>" );
      $("table#restauranttable").find('caption').remove();
    });
      

      //set date piker
      $( "#datepicker" ).datepicker({
        dateFormat: "yy-mm-dd"
      });
      // filter form processing
    $( "#filterForm" ).submit(function(e){
      e.preventDefault();
        var restaurantSelect = $( "#selectRestaurant" ).val();
        var selectdate = $( "#datepicker" ).val();
       
        if( restaurantSelect == "" && selectdate == "" )
        {
          alert( 'Please Select Any One Filter' );
          dataTable.ajax.reload();
        }else{
          
          dataTable.ajax.reload();
        }
    });
    // reset filter form
     $( '#clearFilter' ).click(function(){
      $('#datepicker').val('');
      $( "#selectRestaurant" ).val('');
          dataTable.ajax.reload();
        });
  // show datatable on page load
 
    var dataTable =   $('#restauranttable').DataTable({
        "processing":true,  
           "serverSide":true,  
           "order":[],  
           "ajax":{  
                type:"POST",  
                url: base_url + 'admin/ajax/fetch_restaurant',
                data: function(data){
                          // Read values
                          var date = $('#datepicker').val();
                          var restaurant = $( "#selectRestaurant" ).val();
                          // Append to data
                          data.filter_date = date;
                          data.filter_restaurant = restaurant;
                       }
                 
      
           },  
           "columnDefs":[  
             
                { "targets"                   : 7, "orderable":false}, 
                { "width"                     : "10%", "targets": 7 } 
           ],  
      }); 
  
       

      // set restaurant in filter dropdown
      setRestautant( "" , "selectRestaurant");
      function setRestautant( r_id = "", id_var = "selectRestaurant", ajax_url = "getRestaurantNameId" )
       {

         $.ajax({
                url: base_url + 'admin/ajax/'+ajax_url,
                type: 'POST',
                dataType: "json",
                success:function(response){
                  if( response.status == true )
                  {
                    $.each(response.data, function(key, value){
                      
                        $("#"+id_var).append(new Option(value.rd_restaurant_name, value.rd_id));
                      
                      });
                  }
                }
          });
        }


      $( document ).on('click','.edit',function() {
        var id = $(this).attr('id');
        window.location = base_url + 'admin/restaurant/restaurant/restaurantAction/'+id; 
      });

      // delete user procesing
      $( document ).on('click','.delete',function() {
        var id = $(this).attr('id');
          swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this Restaurant Data!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    deleteRestaurant( id );
                    
                  } else {
                    swal("Restaurant is safe!");
                  }
                });
      });

      //delete Restaurant helper function
      function deleteRestaurant( id ){
          $.ajax({
            url: base_url + 'admin/restaurant/restaurant/deleteRestaurant',
            type: 'POST',
            data: { restaurant_id: id },
            success:function(response){
              if( response == 1 )
              {
                 var dataTable =   $('#restauranttable').DataTable();
                dataTable.ajax.reload();
                 swal("Restaurant has been deleted!", {
                      icon: "success",
                    });
              }else{
                swal("Restaurant is safe!");
              }
            }
          });   
      }


      if($('#restaurantId' ).val())
      {
        var hasId = 'Updated';
      }else{
         var hasId = 'Added';
      }
      // add restaurant form validation
     $( "#restaurantForm" ).submit(function(e){
      e.preventDefault();
          var form = $(this);
          //ajax validate and submit form
          $.ajax({
            url:form.attr('action'),
            type:'post',
            data:form.serialize(),
            dataType: "json",
            success:function(response){
              if( response.success == true )
              {
                if( response.message.error == true )
                {

                  swal("Restaurant "+ hasId +" Successfully!", {
                      icon: "success",
                    })
                  .then((willDelete) => {
                  if (willDelete) {
                    location.reload();
                    
                  } 
                });

                }else{
                  swal("Restaurant "+ hasId +" Error!", {
                      icon: "error",
                    });
                }
                //remove error massages
                $( '.text-danger' ).remove();
                $( '.form-group' ).removeClass( 'has-error' )
                                  .removeClass( 'has-success' );
                form[0].reset();
                 
                 
              }else{
                $.each(response.message, function(key, value){
                  var element = $('#'+key);
                  element.closest('div.form-group')
                  .removeClass( 'has-error' )
                  .addClass( value.length > 0 ? 'has-error' : 'has-success')
                  .find( '.text-danger' )
                  .remove();
                  element.after(value);
                });
              }
            } 
          });


     });


     // show model on show icon click
    $( document ).on('click','.show',function() {
        var id = $(this).attr( 'id' );
        // document.location.href = base_url + 'admin/show/show';
          $.ajax({
            url: base_url + 'admin/restaurant/restaurant/getRestaurantById',
            type: 'POST',
            data: { restaurant_id: id },
            success:function(response){
              var data = JSON.parse( response );
              $( '#show_r_name' ).html( data.rd_restaurant_name );
              $( '#show_o_name' ).html( data.d_restaurant_owner_name );
              $( '#show_o_email' ).html( data.rd_restaurant_email );
              $( '#show_o_mobile' ).html( data.rd_mobile_number );
              $( '#show_r_address' ).html( data.rd_address );
              $( '#show_r_date' ).html( data.rd_inserted_date );
            }
          }); 
      });
 });