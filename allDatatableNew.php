<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Itemtable_model extends CI_Model {
 
    var $table = 'item_master';
    var $column_order = array("im_id", "im_name", "im_item_id", "im_qty", "im_amout", "im_total_amout", "im_order_id", "im_inserted_date", null); //set column field database for datatable orderable
    var $column_search = array('im_name','im_qty','im_amout','im_total_amout','im_inserted_date'); //set column field database for datatable searchable 
    var $order = array('im_id' => 'asc'); // default order 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query()
    {
         
        //add custom filter here
       
        if($this->input->post('filter_date'))
        {
            $this->db->like('im_inserted_date', $this->input->post('filter_date'));
        }
        if($this->input->post('filter_itemOrderId'))
        {
            $this->db->where('im_order_id', $this->input->post('filter_itemOrderId'));
        }
     
        $this->db->from($this->table);
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
  
 
}






user model 


<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Usertable_model extends CI_Model {
 
    var $table = 'user_master';
    var $column_order = array("um_id", "um_name", "um_email", "um_mobile", "um_rd_id", "um_type", "um_inserted_date", null); //set column field database for datatable orderable
    var $column_search = array('um_name','um_email','um_mobile','um_type','um_inserted_date'); //set column field database for datatable searchable 
    var $order = array('um_id' => 'asc'); // default order 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query()
    {
          
            $this->db->where( 'um_type !=', 'superadmin' );
           if( getUser_s() != 'superadmin'  )
           {
            $this->db->where( 'um_rd_id ', getRestaurantId_s() );
           }
        //add custom filter here
       
        if($this->input->post('filter_user'))
        {
            $this->db->like('um_type', $this->input->post('filter_user'));
        }
        if($this->input->post('filter_restaurant'))
        {
            $this->db->where('um_rd_id', $this->input->post('filter_restaurant'));
        }
     
        $this->db->from($this->table);
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
  
 
}

restaurant model 


<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Restauranttable_model extends CI_Model {
 
    var $table = 'restaurant_detail';
    var $column_order = array("rd_id", "rd_restaurant_name", "rd_restaurant_owner_name", "rd_restaurant_email", "rd_mobile_number", "rd_address", "rd_inserted_date", null); //set column field database for datatable orderable
    var $column_search = array('rd_restaurant_name','rd_restaurant_owner_name','rd_restaurant_email','rd_mobile_number','rd_address', 'rd_inserted_date'); //set column field database for datatable searchable 
    var $order = array('rd_id' => 'asc'); // default order 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query()
    {
          
         
        //add custom filter here
       
        if($this->input->post('filter_restaurant'))
        {
            $this->db->where('rd_id', $this->input->post('filter_restaurant'));
        }
        if($this->input->post('filter_date'))
        {
            $this->db->like('rd_inserted_date', $this->input->post('filter_date'));
        }
     
        $this->db->from($this->table);
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
  
 
}



menu model 


<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Menutable_model extends CI_Model {
 
    var $table = 'menu_master';
    var $column_order = array("menu_id", "menu_name", "menu_price", "menu_rd_id", "menu_inserted", null); //set column field database for datatable orderable
    var $column_search = array('menu_name','menu_price','menu_inserted'); //set column field database for datatable searchable 
    var $order = array('menu_id' => 'asc'); // default order 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query()
    {
          
         if( getUser_s() != 'superadmin'  )
         {
          $this->db->where( 'menu_rd_id ', getRestaurantId_s() );
         }
        //add custom filter here
       
        if($this->input->post('filter_restaurant'))
        {
            $this->db->where('menu_rd_id', $this->input->post('filter_restaurant'));
        }
        if($this->input->post('filter_date'))
        {
            $this->db->like('menu_inserted', $this->input->post('filter_date'));
        }
     
        $this->db->from($this->table);
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
  
 
}

table model 



<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Tabletable_model extends CI_Model {
 
    var $table = 'table_master';
    var $column_order = array("tm_id", "tm_name", "tm_rd_id", "inserted_date", null); //set column field database for datatable orderable
    var $column_search = array('tm_name','inserted_date'); //set column field database for datatable searchable 
    var $order = array('tm_id' => 'asc'); // default order 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query()
    {
          
         if( getUser_s() != 'superadmin'  )
         {
          $this->db->where( 'tm_rd_id ', getRestaurantId_s() );
         }
        //add custom filter here
       
        if($this->input->post('filter_restaurant'))
        {
            $this->db->where('tm_rd_id', $this->input->post('filter_restaurant'));
        }
        if($this->input->post('filter_date'))
        {
            $this->db->like('inserted_date', $this->input->post('filter_date'));
        }
     
        $this->db->from($this->table);
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
  
 
}
order model 

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Ordertable_model extends CI_Model {
 
    var $table = 'order_master';
    var $column_order = array("om_id", "om_date", "om_table_name", "om_customer_name", "om_mobile", "om_total_item", "om_total_amount", "om_rd_id", "om_status", "om_user_name", "om_inserted_date", null); //set column field database for datatable orderable
    var $column_search = array('om_date','om_table_name','om_customer_name','om_mobile','om_total_item','om_total_amount','om_status','om_user_name','om_inserted_date'); //set column field database for datatable searchable 
    var $order = array('om_id' => 'asc'); // default order 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query()
    {
          
         if( getUser_s() != 'superadmin'  )
         {
          $this->db->where( 'om_rd_id ', getRestaurantId_s() );
         }
        //add custom filter here
       
        if($this->input->post('filter_restaurant'))
        {
            $this->db->where('om_rd_id', $this->input->post('filter_restaurant'));
        }
        if($this->input->post('filter_date'))
        {
            $this->db->like('om_date', $this->input->post('filter_date'));
        }
     
        $this->db->from($this->table);
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
 
  
 
}