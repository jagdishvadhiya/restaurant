

      <!-- check is add new or edit form -->
<?php
  if( isset( $tableData ) && !empty( $tableData ) ){
      $cardTitle = "Edit ";
      $formId = 'editForm';
      $table     = $tableData[0];
      $tableName = $table[ 'tm_name' ];
      $tableRestaurantId    = $table[ 'tm_rd_id' ];
  }
?> 
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Dashborad</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php admin_c( 'dashboard' ); ?>">Dashborad</a></li>
            <li class="breadcrumb-item"><a href="<?php admin_c( 'table/table' ); ?>">Table</a></li>
            <li class="breadcrumb-item"> <?php if( isset( $cardTitle ) ){echo $cardTitle;}else{ echo 'Add New'; } ?> table</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <!-- jquery validation -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                    <?php if( isset( $cardTitle ) ){echo $cardTitle;}else{ echo 'Add New'; } ?>
                    <small>
                    table
                    </small>
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form role="form" id="tableForm" action="<?php table_v( 'table/formAction' ); ?>" method="post">
                    
                    <div class="card-body">

                      <?php
                      if( isset( $table ) )
                      {
                      echo '<input type="hidden" name="tableId" id="tableId" value="'.$table[ 'tm_id' ].'">';
                      echo '<input type="hidden" name="tableRestaurantId" id="tableRestaurantId" value="'.$table[ 'tm_rd_id' ].'">';
                      }
                      ?>
                      <div class="form-group">
                        <label for="tableName">Table Name</label>
                        <input type="text" name="tableName" class="form-control " id="tableName" placeholder="Enter Table Name" value="<?php if( isset( $tableName ) ){echo $tableName;} ?>">
                        
                      </div>
                      
                    
                      
                     
                      <?php if( getUser_s() == 'superadmin' ){ ?>
                      
                      <div class="form-group ">
                        <label for="selectRestaurant">Restaurant</label>
                        <select name="selectRestaurant" id="selectRestaurant" class="form-control">
                          <option value="">Select Restaurant</option>
                          
                        </select>
                      </div>
                         <?php } ?>
                           <?php if( getUser_s() == 'admin' ){ ?>
                            <input type="hidden" name="selectRestaurant" value="<?php echo getRestaurantId_s(); ?>">
                              <?php } ?>
                      
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="formSubmit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
            </div><!-- /.container-fluid -->
          </div>

         </div> <!-- /.content -->
          <!-- load footer View