<?php
$userRole = $this->session->userdata( 'user_type' );
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo ( $title )? $title : "Restaurant Admin"; ?></title>
    <link rel="icon" href="<?php templat_url(); ?>/images/icon.png">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?php templat_url(); ?>plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php templat_url(); ?>dist/css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php templat_url(); ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php templat_url(); ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php templat_url(); ?>plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/TableExport/5.2.0/css/tableexport.min.css">
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/> -->
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.bootstrap4.min.css"/> -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/TableExport/3.2.5/css/tableexport.min.css">
    <?php $this->load->view('admin/templat/header-extra'); ?>
    <script>
    var base_url = '<?php echo base_url(); ?>';
    //   console.log( base_url );
    </script>
  </head>
  <body class="hold-transition sidebar-mini">
    
    <div class="wrapper">
      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
          </li>
          
        </ul>
        <ul class="navbar-nav">
          <li class="nav-item">
            <span class="h4 text-primary">
              <?php echo ucfirst( getRestaurantName_s() ); ?>
            </span>
          </li>
        </ul>
        <!-- SEARCH FORM -->
        
        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <span class="h5">Welcome <span class="h4 text-primary"><?php echo ucfirst( getUserName_s() ); ?></span></span>
          </li>
          
          <li class="nav-item dropdown show">
            <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="true">
              <i class="far fa-user "></i>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right " style="left: inherit; right: 0px;">
              
              
              <div class="col m-2">
                <center>
                <span class="mr-3 h3 mt-3"> <?php echo strtoupper( getUserName_s() ); ?></span>
                </center>
              </div>
              <div class="dropdown-divider"></div>
              <?php if( $userRole == 'superadmin' ){ ?>
              <a href="<?php admin_c( 'profile' ); ?>" class="dropdown-item">
                <i class="fas fa-cog mr-2"></i> Profile Settings
              </a>
              <?php } ?>
              <?php if( $userRole == 'admin' ){ ?>
              <a href="<?php admin_c( 'profile' ); ?>" class="dropdown-item">
                <i class="fas fa-cog mr-2"></i> Restaurant Settings
              </a>
              <?php } ?>
              
              <a href="<?php echo base_url().'admin/login/logout' ?>" class="dropdown-item">
                <i class="fas fa-sign-out-alt mr-2"></i> Logout
              </a>
              
            </li>
          </ul>
        </nav>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
          <!-- Brand Logo -->
          <a href="index3.html" class="brand-link">
            <img src="<?php templat_url(); ?>/images/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
            style="opacity: .8">
            <span class="brand-text font-weight-light">Restaurant Admin</span>
          </a>
          <!-- Sidebar -->
          <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
              <div class="image">
                <img src="<?php templat_url(); ?>dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
              </div>
              <div class="info">
                <?php if( getUser_s() != 'user' ){ ?>
                <a href="<?php admin_c( 'profile' ); ?>" class="d-block"> <?php echo strtoupper( getUser_s() ); ?> </a>
              <?php }else{ ?>
                <a href="" class="d-block"> <?php echo strtoupper( getUser_s() ); ?> </a>
              <?php } ?>
              </div>
            </div>
            <!-- Sidebar Menu -->
            <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
                <li class="nav-item">
                  <a href="<?php admin_c( 'dashboard' ); ?>" class="nav-link ">
                    <i class="nav-icon fas fa-th"></i>
                    <p>
                      Dashboard
                    </p>
                  </a>
                </li>
                <li class="nav-item has-treeview ">
                  <a href="#" class="nav-link ">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                      User
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item ">
                      <a href="<?php user_v( 'user' ); ?>" class="nav-link ">
                        <i class="far fa-circle nav-icon"></i>
                        <p>All Users</p>
                      </a>
                    </li>
                    <?php if( getUser_s() != 'user' ){ ?>
                    <li class="nav-item">
                      <a href="<?php user_v( 'user/userAction' ); ?>" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Add User</p>
                      </a>
                    </li>
                  <?php } ?>
                    
                  </ul>
                </li>
                <?php if( getUser_s() == 'superadmin' ){ ?>
                <li class="nav-item has-treeview ">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                      Restaurants
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="<?php restaurant_v( 'restaurant' ); ?>" class="nav-link ">
                        <i class="far fa-circle nav-icon"></i>
                        <p>All Restaurant</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="<?php restaurant_v( 'restaurant/restaurantAction' ); ?>" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Add Restaurant</p>
                      </a>
                    </li>
                    
                  </ul>
                </li>
              <?php } ?>
                <li class="nav-item has-treeview ">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                      Menus
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="<?php menu_v( 'menu' ); ?>" class="nav-link ">
                        <i class="far fa-circle nav-icon"></i>
                        <p>All Menus</p>
                      </a>
                    </li>
                     <?php if( getUser_s() != 'user' ){ ?>
                    <li class="nav-item">
                      <a href="<?php menu_v( 'menu/menuAction' ); ?>" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Add Menu</p>
                      </a>
                    </li>
                      <?php } ?>
                  </ul>
                </li>
                <li class="nav-item has-treeview ">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                      Tables
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="<?php table_v( 'table' ); ?>" class="nav-link ">
                        <i class="far fa-circle nav-icon"></i>
                        <p>All Tables</p>
                      </a>
                    </li>
                     <?php if( getUser_s() != 'user' ){ ?>
                    <li class="nav-item">
                      <a href="<?php table_v( 'table/tableAction' ); ?>" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Add table</p>
                      </a>
                    </li>
                      <?php } ?>
                  </ul>
                </li>
                <li class="nav-item has-treeview ">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                      Orders
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="<?php order_v( 'order' ); ?>" class="nav-link ">
                        <i class="far fa-circle nav-icon"></i>
                        <p>All Orders</p>
                      </a>
                    </li>
                     <?php if( getUser_s() != 'user' ){ ?>
                    <li class="nav-item">
                      <a href="<?php order_v( 'order/orderAction' ); ?>" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Add order</p>
                      </a>
                    </li>
                      <?php } ?>
                  </ul>
                </li>
              </li>
              <li class="nav-item has-treeview ">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                    Items
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?php item_v( 'item' ); ?>" class="nav-link ">
                      <i class="far fa-circle nav-icon"></i>
                      <p>All Items</p>
                    </a>
                  </li>
                </ul>
              </li>
              
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">