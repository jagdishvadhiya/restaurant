

      <!-- check is add new or edit form -->
<?php
  if( isset( $menuData ) && !empty( $menuData ) ){
      $cardTitle = "Edit ";
      $formId = 'editForm';
      $menu     = $menuData[0];
      $menuName = $menu[ 'menu_name' ];
      $menuPrice     = $menu[ 'menu_price' ];
      $menuRestaurantId    = $menu[ 'menu_rd_id' ];
  }
?> 
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Dashborad</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php admin_c( 'dashboard' ); ?>">Dashborad</a></li>
            <li class="breadcrumb-item"><a href="<?php admin_c( 'menu/menu' ); ?>">Menu</a></li>
            <li class="breadcrumb-item"> <?php if( isset( $cardTitle ) ){echo $cardTitle;}else{ echo 'Add New'; } ?> menu</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <!-- jquery validation -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                    <?php if( isset( $cardTitle ) ){echo $cardTitle;}else{ echo 'Add New'; } ?>
                    <small>
                    menu
                    </small>
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form role="form" id="menuForm" action="<?php menu_v( 'menu/formAction' ); ?>" method="post">
                    
                    <div class="card-body">

                      <?php
                      if( isset( $menu ) )
                      {
                      echo '<input type="hidden" name="menuId" id="menuId" value="'.$menu[ 'menu_id' ].'">';
                      echo '<input type="hidden" name="menuRestaurantId" id="menuRestaurantId" value="'.$menu[ 'menu_rd_id' ].'">';
                      }
                      ?>
                      <div class="form-group">
                        <label for="menuName">Name</label>
                        <input type="text" name="menuName" class="form-control " id="menuName" placeholder="Enter Menu Name" value="<?php if( isset( $menuName ) ){echo $menuName;} ?>">
                        
                      </div>
                      
                      <div class="form-group">
                        <label for="menuPrice">Price</label>
                        <input type="number" name="menuPrice" class="form-control " id="menuPrice" placeholder="Enter menu Price"value="<?php if( isset( $menuPrice ) ){echo $menuPrice;} ?>">
                        
                      </div>
                      
                    
                      
                       <?php if( getUser_s() == 'superadmin' ){ ?>
                      
                      <div class="form-group ">
                        <label for="selectRestaurant">Restaurant</label>
                        <select name="selectRestaurant" id="selectRestaurant" class="form-control">
                          <option value="">Select Restaurant</option>
                          
                        </select>
                      </div>
                         <?php } ?>
                           <?php if( getUser_s() == 'admin' ){ ?>
                            <input type="hidden" name="selectRestaurant" value="<?php echo getRestaurantId_s(); ?>">
                              <?php } ?>
                      
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="formSubmit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
            </div><!-- /.container-fluid -->
          </div>

         </div> <!-- /.content -->
          <!-- load footer View