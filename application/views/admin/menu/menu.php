<!-- load header View -->
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Menus</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php admin_c( 'dashboard' ); ?>">Dashborad</a></li>
            <li class="breadcrumb-item">Menus</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-4"><h3 class="card-title">Menu List</h3>
                      <div class="dropdown">
                           <button class="btn btn-success py-0 float-right dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file-download" ></i></button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="someOtherDiv">
                              <a id="pdfExport" class="btn btn-block pdf p-0"  style="background-color: rgb(233, 236, 239);"><i class="fas fa-file-pdf m-0 p-2 py-2 bg-danger ml-1"></i><span class="ml-2">Export to pdf</span></a>
                            </div>
                          </div>
                        </div>
                      <div class="col-8">
                        
                        <form action="" method="post" id="filterForm">
                          <div class="row ml-5">
                        
                            <div class="col-5 float-right">
                               <?php if( getUser_s() == 'superadmin' ){ ?>
                              <select name="selectRestaurant" id="selectRestaurant" class="form-control">
                                <option value="">Select Restaurant</option>
                              </select>
                            <?php } ?>
                            </div>  
                              <div class="col-5 float-right">
                               <input type="text" name="selectdate" class="form-control" id="datepicker" placeholder="Select Date">
                             </div>
                            <div class="col-2 float-right">
                              <button type="submit" class="btn btn-primary p-2"><i class="fa fa-filter" aria-hidden="true"></i>
</button>
                              <button type="reset" class="btn btn-primary p-2"  id='clearFilter'><i class="fa fa-times" aria-hidden="true"></i>
</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table id="menutable"  class=" table table-bordered table-hover " style="width:100%">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Name</th>
                          <th>Price(&#8377)</th>
                          <th>Restaurant Name</th>
                          <th>Register Date</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      
                      <tfoot>
                      <tr class="noExl">
                        <th>Id</th>
                        <th>Name</th>
                        <th>Price(&#8377)</th>
                        <th>Restaurant Name</th>
                        <th>Register Date</th>
                        <th>Action</th>
                      </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.col -->
            </div>
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content -->
          <!-- load footer View -->




          <div class="modal fade" id="showmodel">
            <div class="modal-dialog">
              <div class="modal-content bg-light">
                <div class="modal-header">
                  <h4 class="modal-title">Restaurant View</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body justify-content-center">
                            <div class="row">
                              <div class="col">
                                <table class="table">
                                  
                                  <tbody>
                                      <tr>
                                    <th scope="row">Menu Name</th>
                                    <td>:</td>
                                    <td id="show_menu_name">Pizza</td>
                                  </tr>
                                   <tr>
                                    <th scope="row">Price</th>
                                    <td>:</td>
                                    <td id="show_menu_price">200 Rs.</td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Restaurant Name</th>
                                    <td>:</td>
                                    <td id="show_menu_r_name">Jk Restaurant </td>
                                  </tr>
                                  
                                    <tr>
                                    <th scope="row">Registration Date</th>
                                    <td>:</td>
                                    <td id="show_menu_date">200-10-02 00:00:00</td>
                                  </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
        <!-- /.content -->
          <!-- model for filters -->