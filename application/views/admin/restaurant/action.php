<!-- check is add new or edit form -->
<?php 
if( isset( $restaurantData ) && !empty( $restaurantData ) ){
    $cardTitle = "Edit ";
    $formId = 'editForm';
    $restaurant     = $restaurantData[0];
    $restaurantName = $restaurant[ 'rd_restaurant_name' ];
    $ownerName      = $restaurant[ 'rd_restaurant_owner_name' ];
    $ownerEmail     = $restaurant[ 'rd_restaurant_email' ];
    $ownerMobile    = $restaurant[ 'rd_mobile_number' ];
    $ownerAddress   = $restaurant[ 'rd_address' ];
   
}
?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Dashborad</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php admin_c( 'dashboard' ); ?>">Dashborad</a></li>
            <li class="breadcrumb-item"><a href="<?php admin_c( 'restaurant/restaurant' ); ?>">Restaurant</a></li>
            <li class="breadcrumb-item"> <?php if( isset( $cardTitle ) ){echo $cardTitle;}else{ echo 'Add New'; } ?> Restaurant</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <!-- jquery validation -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                    <?php if( isset( $cardTitle ) ){echo $cardTitle;}else{ echo 'Add New'; } ?>
                    <small>
                    Restaurant
                    </small>
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form role="form" id="restaurantForm" action="<?php restaurant_v( 'restaurant/formAction' ); ?>" method="post">
                  
                    <div class="card-body">
                        <?php 
                      if( isset( $restaurant ) )
                      {
                         echo '<input type="hidden" name="restaurantId" id="restaurantId" value="'.$restaurant[ 'rd_id' ].'">';
                      }
                     ?>
                      <div class="form-group">
                        <label for="restaurantName">Restaurant Name</label>
                        <input type="text" name="restaurantName" class="form-control " id="restaurantName" placeholder="Enter Restaurant Name" value="<?php if( isset( $restaurantName ) ){echo $restaurantName;} ?>">
                        
                      </div>
                      
                      <div class="form-group">
                        <label for="ownerName">Owner Name</label>
                        <input type="text" name="ownerName" class="form-control " id="ownerName" placeholder="Enter Owner Name"value="<?php if( isset( $ownerName ) ){echo $ownerName;} ?>">
                        
                      </div>
                      
                      <div class="form-group">
                        <label for="ownerEmail">Owner Email</label>
                        <input type="email" name="ownerEmail" class="form-control " id="ownerEmail" placeholder="Enter Owner Email"value="<?php if( isset( $ownerEmail ) ){echo $ownerEmail;} ?>">
                        
                      </div>
                      <div class="form-group">
                        <label for="ownerMobile">Owner Mobile Number</label>
                        <input type="text" name="ownerMobile" class="form-control " id="ownerMobile" placeholder="Enter Owner Mobile Number"value="<?php if( isset( $ownerMobile ) ){echo $ownerMobile;} ?>">
                        
                      </div>
                       <div class="form-group">
                        <label for="ownerPassword">Password</label>
                        <input type="password" name="ownerPassword" class="form-control " id="ownerPassword" placeholder="Enter Password" >
                      </div>
                      <div class="form-group">
                        <label for="ConfirmPassword">Confirm Password</label>
                        <input type="password" name="confirmPassword" class="form-control " id="confirmPassword" placeholder="Confirm Password" >
                      </div>
                      <div class="form-group">
                        
                        <label for="ownerAddress">Owner Address</label>
                        <textarea class="form-control " name="ownerAddress" id="ownerAddress"aria-label="With textarea" placeholder="Enter Address"><?php if( isset( $ownerAddress ) ){echo $ownerAddress;} ?></textarea></br>
                        
                      </div>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="formSubmit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
            </div><!-- /.container-fluid -->
          </div>
        </div>  <!-- /.content -->
          <!-- load footer View