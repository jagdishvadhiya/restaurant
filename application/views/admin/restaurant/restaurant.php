<!-- load header View -->
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Restaurants</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php admin_c( 'dashboard' ); ?>">Dashborad</a></li>
            <li class="breadcrumb-item">Restaurants</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-4">
                        <h3 class="card-title">Restaurant List</h3>
                          <div class="dropdown">
                           <button class="btn btn-success py-0 float-right dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file-download" ></i></button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="someOtherDiv">
                            </div>
                          </div>
                        </div>
                      
                      <div class="col-8">
                        
                        <form action="" method="post" id="filterForm">
                          <div class="row ">
                            <div class="col-5 float-right">
                              <input type="text" name="selectdate" class="form-control" id="datepicker" placeholder="Select Date">
                            </div>
                            <div class="col-5 float-right">
                              <select name="selectRestaurant" id="selectRestaurant" class="form-control">
                                <option value="">Select Restaurant</option>
                              </select>
                            </div>
                            
                            <div class="col-2 float-right">
                              <button type="submit" class="btn btn-primary"><i class="fa fa-filter" aria-hidden="true"></i>
                              </button>
                              <button type="reset" class="btn btn-primary"  id='clearFilter'><i class="fa fa-times" aria-hidden="true"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                      </div>
                    </div>
                  
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table id="restauranttable"  class=" table table-bordered table-hover " style="width:100%">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Name</th>
                          <th>Owner Name</th>
                          <th>Email </th>
                          <th>Mobile Number</th>
                          <th>Address</th>
                          <th>Register Date</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      
                      <tfoot>
                      
                        <tr>
                          <th>Id</th>
                          <th>Name</th>
                          <th>Owner Name</th>
                          <th>Email </th>
                          <th>Mobile Number</th>
                          <th>Address</th>
                          <th>Register Date</th>
                          <th>Action</th>
                        </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
                </div>
                <!-- /.col -->
              </div>

              </div><!-- /.container-fluid -->
     
            <!-- /.content -->
            <!-- load footer View -->
            <div class="modal fade" id="showmodel">
              <div class="modal-dialog">
                <div class="modal-content bg-light">
                  <div class="modal-header">
                    <h4 class="modal-title">Restaurant View</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                  </div>
                  <div class="modal-body justify-content-center">
                    <div class="row">
                      <div class="col">
                        <table class="table">
                          
                          <tbody>
                            <tr>
                              <th scope="row">Restaurant Name</th>
                              <td>:</td>
                              <td id="show_r_name">Jk Restaurant</td>
                            </tr>
                            <tr>
                              <th scope="row">Owner Name</th>
                              <td>:</td>
                              <td id="show_o_name">Jagdish Vadhiya</td>
                            </tr>
                            <tr>
                              <th scope="row">Email</th>
                              <td>:</td>
                              <td id="show_o_email">Jk@jk.com</td>
                            </tr>
                            <tr>
                              <th scope="row">Mobile Number</th>
                              <td>:</td>
                              <td id="show_o_mobile">89809890980</td>
                            </tr>
                            <tr>
                              <th scope="row">Address</th>
                              <td>:</td>
                              <td id="show_r_address">Rajkot, kothariya</td>
                            </tr>
                            <tr>
                              <th scope="row" >Registration Date</th>
                              <td>:</td>
                              <td id="show_r_date">200-10-02 00:00:00</td>
                            </tr>
                            
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.content -->
            <!-- model for filters -->