
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $title; ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php templat_url(); ?>plugins/fontawesome-free/css/all.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="<?php templat_url(); ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php templat_url(); ?>dist/css/adminlte.min.css">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	<script>
    var base_url = '<?php echo base_url(); ?>';
 //   console.log( base_url );
</script>
</head>
<body class="hold-transition login-page bg-primary">

	<div class="login-box">
		
		<!-- /.login-logo -->
		<div class="card">
			<div class="card-header" ><div class="login-logo">
				<h3 class="text-primary">Admin Login</h3>
			</div>
		</div>
		<div class="card-body login-card-body">
			<p class="login-box-msg">Sign in to start your session</p>

			<!-- <form action="<?php echo base_url(); ?>admin/login/login" method="post"> -->
			<form action="" method="post">
				<div class="input-group mb-1">
					<input type="email" name="email" class="form-control" placeholder="Email" id="email" required>
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-envelope"></span>
						</div>
					</div>
					
				</div>
				<div class="row  ">
					<div id="emailerror" class="col ml- text-danger errors" style=" display: none;">Email Invalid</div>
				</div>
				<div class="input-group mb-3">
					<input type="password" name="password" class="form-control" placeholder="Password" id="password" required>
					<div class="input-group-append">
						<div class="input-group-text">
							<span class="fas fa-lock"></span>
						</div>
					</div>
				</div>
				<div class="row  ">
					<div id="passerror" class="col ml- text-danger errors" style=" display: none;">Password Invalid</div>
				</div>
				<div class="row">
					
					<!-- /.col -->
					<div class="col">
						<button type="submit" name="login" id="login" class="btn btn-primary btn-block" value="Login">Sign In</button>
					</div>
					<!-- /.col -->
				</div>
			</form>



		</div>
		<!-- /.login-card-body -->
	</div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?php templat_url(); ?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php templat_url(); ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php templat_url(); ?>dist/js/adminlte.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxy/1.6.1/scripts/jquery.ajaxy.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php templat_url(); ?>custom/js/admin-login-ajax.js"></script>

</body>
</html>
