<?php
// // print_r($this->session->userdata());
// echo "<pre>";
// print_r($profile['profileData']);
?>

<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Profile</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php admin_c( 'dashboard' ); ?>">Dashborad</a></li>
            <li class="breadcrumb-item">Profile Settings</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <section class="content">
          <div class="container-fluid">
            <div class="row">
              
              <!-- /.col -->
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header p-2">
                    <ul class="nav nav-pills">
                      <li class="nav-item"><a class="nav-link active" href="#userRole" data-toggle="tab">User Role</a></li>
                      <li class="nav-item"><a class="nav-link " href="#settings" data-toggle="tab">Settings</a></li>
                    </ul>
                    </div><!-- /.card-header -->
                    <div class="card-body">
                      <div class="tab-content">
                        <div class="tab-pane active" id="userRole">
                          <!-- Post -->
                          <div class="row">
                            <div class="col">
                              <h3>Change Roles</h3>
                            </div>
                              <div class="col-9 text-success bg-light py-0">
                                <center>
                                <div class="alert alert-success my-0" id="premitionMsg" role="alert">
                                  Permition Updated Successfully...
                                </div>
                                </center>
                              </div>
                          </div>
                          <div class="row">
                            <div class="col">
                              <table class="table table-bordered table-responsve text-center">
                                <thead>
                                  <tr class="bg-dark">
                                    <th scope="col">No</th>
                                    <th scope="col">User Name</th>
                                    <th scope="col">User Role</th>
                                    <th scope="col">Delte</th>
                                    <th scope="col">Edit</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach ($profile['userData'] as $key => $value) { ?>
                                    
                                    <tr>
                                    <th scope="row"><?php echo $key+1; ?></th>
                                    <td><?php echo  $value['um_name']; ?></td>
                                    <td><?php echo  $value['um_type']; ?></td>
                                   <td>
                                         <?php if( getDeletePermition( $value['um_id'] ) ){ $checked = 'checked'; }else{ $checked = ''; } ?>

                                      <input type="checkbox" class="checkbox" name="delete" id="delete" data-utype="<?php echo  $value['um_type'] ?>" value="<?php echo  $value['um_id'] ?>" <?php echo $checked ?> >
                                    </td>
                                    <td>
                                       <?php if( getEditPermition( $value['um_id'] ) ){ $checked = 'checked'; }else{ $checked = ''; } ?>
                                      <input type="checkbox" class="checkbox" name="edit" id="edit" data-utype="<?php echo  $value['um_type'] ?>" value="<?php echo  $value['um_id'] ?>" <?php echo $checked ?> >
                                    </td>
                                  </tr>

                               <?php   } ?>
                                  
                                 
                                </tbody>
                              </table>
                            </div>
                            
                            <!-- /.user-block -->
                            
                          </div>
                          <!-- /.post -->
                          
                          
                        </div>
                        
                        <div class="tab-pane " id="settings">
                          <!-- for admin form -->
                            <?php if ( getUser_s() == 'admin' ) { ?>
                          <form class="form-horizontal" id="profileform" method="post" action="<?php admin_c( 'profile/profileAdminUpdate' ); ?>">
                    
                           
                            <input type="hidden" name="restaurantId" value="<?php echo getRestaurantId_s(); ?>">
                            <div class="form-group row">
                              <label for="restaurantName" class="col-sm-2 col-form-label">Restaurant Name</label>
                              <div class="col-sm-10">
                                <input type="text" name="restaurantName" class="form-control" id="restaurantName" placeholder="Restaurant Name"  value="<?php echo getRestaurantName_s(); ?>">
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="ownerName" class="col-sm-2 col-form-label">Owner Name</label>
                              <div class="col-sm-10">
                                <input type="text" name="ownerName" class="form-control" id="ownerName" placeholder="Owner Name"
                                value="<?php echo $profile['profileData']['um_name'] ?>">
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="ownerEmail" class="col-sm-2 col-form-label">Email</label>
                              <div class="col-sm-10">
                                <input type="email" name="ownerEmail" class="form-control" id="ownerEmail" placeholder="Email" value="<?php echo $profile['profileData']['um_email'] ?>">
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="ownerMobile" class="col-sm-2 col-form-label">Mobile Number</label>
                              <div class="col-sm-10">
                                <input type="number" name="ownerMobile" class="form-control" id="ownerMobile" placeholder="Enter Mobile Number" value="<?php echo $profile['profileData']['um_mobile'] ?>">
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="ownerAddress" class="col-sm-2 col-form-label">Address</label>
                              <div class="col-sm-10">
                                <textarea class="form-control" name="ownerAddress" id="ownerAddress" placeholder="Enter Address..."><?php echo $profile['restaurantData']['rd_address'] ?></textarea>
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="password" class="col-sm-2 col-form-label">Password</label>
                              <div class="col-sm-10">
                                <input type="password" name="ownerPassword" class="form-control" id="password" placeholder="Enter New Password">
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="confirmPassword" class="col-sm-2 col-form-label">Confirm Password</label>
                              <div class="col-sm-10">
                                <input type="password" name="confirmPassword" class="form-control" id="confirmPassword" placeholder="Enter Password Again">
                              </div>
                            </div>
                          <?php  }  ?>
                            <!-- admin form end -->
                            <!-- Superadmin form start -->
                              <?php if ( getUser_s() == 'superadmin' ) { ?>
                          <form class="form-horizontal" id="profileform" method="post" action="<?php admin_c( 'profile/profileSuperAdminUpdate' ); ?>">
                    
                           
                            <input type="hidden" name="userId" value="<?php echo $profile['profileData']['um_id']; ?>">
                           
                            <div class="form-group row">
                              <label for="ownerName" class="col-sm-2 col-form-label">Super Admin Name</label>
                              <div class="col-sm-10">
                                <input type="text" name="ownerName" class="form-control" id="ownerName" placeholder="Owner Name"
                                value="<?php echo $profile['profileData']['um_name'] ?>">
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="ownerEmail" class="col-sm-2 col-form-label">Email</label>
                              <div class="col-sm-10">
                                <input type="email" name="ownerEmail" class="form-control" id="ownerEmail" placeholder="Email" value="<?php echo $profile['profileData']['um_email'] ?>">
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="ownerMobile" class="col-sm-2 col-form-label">Mobile Number</label>
                              <div class="col-sm-10">
                                <input type="number" name="ownerMobile" class="form-control" id="ownerMobile" placeholder="Enter Mobile Number" value="<?php echo $profile['profileData']['um_mobile'] ?>">
                              </div>
                            </div>
                           
                            <div class="form-group row">
                              <label for="password" class="col-sm-2 col-form-label">Password</label>
                              <div class="col-sm-10">
                                <input type="password" name="ownerPassword" class="form-control" id="password" placeholder="Enter New Password">
                              </div>
                            </div>
                            <div class="form-group row">
                              <label for="confirmPassword" class="col-sm-2 col-form-label">Confirm Password</label>
                              <div class="col-sm-10">
                                <input type="password" name="confirmPassword" class="form-control" id="confirmPassword" placeholder="Enter Password Again">
                              </div>
                            </div>
                          <?php  }  ?>
                            <div class="form-group row">
                              <div class="offset-sm-2 col-sm-10">
                                <button type="submit" class="btn btn-danger">Update</button>
                              </div>
                            </div>
                          </form>
                        </div>
                        <!-- /.tab-pane -->
                      </div>
                      <!-- /.tab-content -->
                      </div><!-- /.card-body -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
                </div><!-- /.container-fluid -->
              </section>


    <div class="toast" id="myToast" style="position: absolute; top: 0; right: 0;">
        <div class="toast-header">
            <strong class="mr-auto"><i class="fa fa-right"></i> Applye Changes!</strong>
            <small>11 mins ago</small>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
       
    </div>
