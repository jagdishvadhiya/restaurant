<?php 
if( isset( $itemOrderId ) && $itemOrderId != '' )
{
  echo "<input type='text' class='itemOrderId d-none' value='$itemOrderId' >";
  }  else{
     echo "<input type='text' class='itemOrderId d-none' >";
  }
 ?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Items</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php admin_c( 'dashboard' ); ?>">Dashborad</a></li>
          <li class="breadcrumb-item">Items</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
         <div class="col-12">
         
            <div class="card">
              <div class="card-header">
                <div class="row">
                      <div class="col-4">
                        <h3 class="card-title">Item List </h3> 
                        <div class="dropdown">
                           <button class="btn btn-success py-0 float-right dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file-download" ></i></button>
                           
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="someOtherDiv">
                             
                            </div>
                          </div>
                       
                      </div>
                      <div class="col-8">
                        
                        <form action="" method="post" id="filterForm">
                          <div class="row float-right">
                           
                            <div class="col-8 float-right">
                              <input type="text" name="selectdate" class="form-control date" id="datepicker" placeholder="Select Date">
                            </div>
                          
                            <div class="col-2 float-right">
                              <button type="submit" class="btn btn-primary"><i class="fa fa-filter" aria-hidden="true"></i></button>
                            </div>
                            <div class="col-2 float-right">
                              <button type="reset" class="btn btn-primary" id='clearFilter'><i class="fa fa-times" aria-hidden="true"></i></button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
             <table id="itemtable"  class=" table table-bordered table-hover " style="width:100%" >
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Menu Name</th>
                        <th>Item Qty</th>
                        <th>Item Amount</th>
                        <th>Item Total Amount</th>
                        <th>Order By</th>
                        <th>Register Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tfoot>
                       <tr  class="noExl">
                         <th>Id</th>
                        <th>Name</th>
                        <th>Item Id</th>
                        <th>Item Qty</th>
                        <th>Item Amount</th>
                        <th>Item Total Amount</th>
                        <th>Order By</th>
                        <th>Register Date</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

          <!-- /.col -->
        </div>
</div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<!-- load footer View -->


<div class="modal fade" id="showmodel">
            <div class="modal-dialog">
              <div class="modal-content bg-light">
                <div class="modal-header">
                  <h4 class="modal-title">Item View</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body justify-content-center">
                            <div class="row">
                              <div class="col">
                                <table class="table">
                                  
                                  <tbody>
                                      <tr>
                                    <th scope="row">Item Name</th>
                                    <td>:</td>
                                    <td id="show_item_name">Pizza</td>
                                  </tr>
                                   <tr>
                                    <th scope="row">Item Amout</th>
                                    <td>:</td>
                                    <td id="show_item_price">200 Rs.</td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Item Quentity</th>
                                    <td>:</td>
                                    <td id="show_item_qty">Jk Restaurant </td>
                                  </tr>
                                  
                                    <tr>
                                    <th scope="row">Item Total</th>
                                    <td>:</td>
                                    <td id="show_item_total">200-10-02 00:00:00</td>
                                  </tr>
                                  <tr>
                                    <th scope="row">Item Ordered By</th>
                                    <td>:</td>
                                    <td id="show_item_order">200-10-02 00:00:00</td>
                                  </tr>
                                    <tr>
                                    <th scope="row">Registerd Date</th>
                                    <td>:</td>
                                    <td id="show_item_date">200-10-02 00:00:00</td>
                                  </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
        <!-- /.content -->
          <!-- model for filters -->
