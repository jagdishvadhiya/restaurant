<!-- check is add new or edit form -->
<?php
if( isset( $itemData ) && !empty( $itemData ) ){
$cardTitle = "Edit ";
$formId = 'editForm';
$item     = $itemData[0];
$itemId = $item[ 'im_id' ];
$itemName = $item[ 'im_name' ];
$itemMenuId = $item[ 'im_item_id' ];
$itemQty = $item[ 'im_qty' ];
$itemAmout = $item[ 'im_amout' ];
$itemTotalAmout     = $item[ 'im_total_amout' ];
$itemOrderId    = $item[ 'im_order_id' ];
}
?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Dashborad</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php admin_c( 'dashboard' ); ?>">Dashborad</a></li>
            <li class="breadcrumb-item"><a href="<?php admin_c( 'item/item' ); ?>">Item</a></li>
            <li class="breadcrumb-item"> <?php if( isset( $cardTitle ) ){echo $cardTitle;}else{ echo 'Add New'; } ?> item</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <!-- jquery validation -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                    <?php if( isset( $cardTitle ) ){echo $cardTitle;}else{ echo 'Add New'; } ?>
                    <small>
                    item
                    </small>
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form role="form" id="itemForm" action="<?php item_v( 'item/formAction' ); ?>" method="post">
                    
                    <div class="card-body">
                      <?php
                      if( isset( $item ) )
                      {
                      echo '<input type="hidden" name="itemId" id="itemId" value="'.$item[ 'im_id' ].'">';
                      echo '<input type="hidden" name="itemMenuId" id="itemMenuId" value="'.$item[ 'im_item_id' ].'">';
                      }
                      ?>
                      <div class="form-group">
                        <label for="itemName">Name</label>
                        <input type="text" name="itemName" class="form-control " id="itemName" placeholder="Enter Item Name" value="<?php if( isset( $itemName ) ){echo $itemName;} ?>">
                        
                      </div>
                      <div class="form-group ">
                        <label for="selectItemMenu">Menu</label>
                     
                          <?php if( isset( $menuData ) && !empty( $menuData ) ){ ?>
                            <?php foreach ($menuData as $key => $value) {
                              # code...
                              if( $item[ 'im_item_id' ] == $value['menu_id'] )
                              {

                              echo ' <input type="text" value="'.$value['menu_name'].'" class="form-control" disabled>';
                              }
                            } ?>
                          <?php } ?>
                  
                      </div>
                      <div class="row">
                        <div class="col-4">
                          <div class="form-group">
                            <label for="itemPrice">Price</label>
                              <input type="hidden" name="itemPrice" class="form-control " id="itemPrice" placeholder="Enter Item Price" value="<?php if( isset( $itemAmout ) ){echo $itemAmout;}else{echo '0';} ?>" min='0'>
                            <input type="number" name="" class="form-control " id="itemPrice" placeholder="Enter Item Price" value="<?php if( isset( $itemAmout ) ){echo $itemAmout;}else{echo '0';} ?>" min='0' disabled>
                            
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="itemQty">Quentity</label>
                            <input type="number" name="itemQty" class="form-control " id="itemQty" placeholder="Enter Item Quentity" value="<?php if( isset( $itemQty ) ){echo $itemQty;}else{echo '1';} ?>" min='1'>
                            
                          </div>
                        </div>
                        <div class="col-4">
                          <div class="form-group">
                            <label for="itemQty">Total Amout</label>
                            <input type="text" name="totalAmout" class="form-control " id="totalAmout" value="<?php if( isset( $itemTotalAmout ) ){echo $itemTotalAmout;}else{echo "0";} ?>" disabled>
                            
                          </div>
                        </div>
                      </div>
                      
                      
                      
                      
                      
                      <div class="form-group ">
                        <label for="selectRestaurant">User Name</label>
                        <input type="hidden" name="itemOrderId" id="itemOrderId" value="<?php if( isset( $itemOrderId ) ){echo $itemOrderId;}else{echo "0";} ?>">
                          <input type="text" name="itemOrderUserName" id="itemOrderUserName" value="" class="form-control" disabled>
                      </div>
                      
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="formSubmit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
            </div><!-- /.container-fluid -->
          </div>
          </div> <!-- /.content -->
          <!-- load footer View