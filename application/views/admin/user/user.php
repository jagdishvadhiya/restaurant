<!-- load header View -->
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Users</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php admin_c( 'dashboard' ); ?>">Dashborad</a></li>
            <li class="breadcrumb-item">Users</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-4">
                        <h3 class="card-title">User List</h3>
                        <div class="dropdown">
                           <button class="btn btn-success py-0 float-right dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file-download" ></i></button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="someOtherDiv">
                            </div>
                          </div>
                      </div>
                      <div class="col-8">
                        
                        <form action="" method="post" id="filterForm">
                          <div class="row ">
                            <div class="col-5 float-right">
                              <?php if( getUser_s() == 'superadmin' ){ ?>
                              <select name="selectRestaurant" id="selectRestaurant" class="form-control">
                                <option value="">Select Restaurant</option>
                              </select>
                              <?php } ?>
                            </div>
                            <div class="col-5 float-right">
                              <select name="selectUser" id="selectUser" class="form-control">
                                <option value="">Select User</option>
                                <?php if( getUser_s() == 'superadmin' ){ ?>
                                <option value="superadmin">Super Admin</option>
                                <?php } ?>
                                <option value="admin">Admin</option>
                                <option value="user">User</option>
                              </select>
                            </div>
                            <div class="col-2 float-right">
                              <button type="submit" class="btn btn-primary"><i class="fa fa-filter" aria-hidden="true"></i>
                              </button>
                              <button type="reset" class="btn btn-primary"  id='clearFilter'><i class="fa fa-times" aria-hidden="true"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table id="usertable"  class=" table table-bordered text-center" style="width:100%">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Mobile</th>
                          <th>Restaurant Name</th>
                          <th>User Type</th>
                          <th>Register Date</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      
                      <tfoot>
                      <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Restaurant Name</th>
                        <th>User Type</th>
                        <th>Register Date</th>
                        <th>Action</th>
                      </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.col -->
            </div>
            </div><!-- /.container-fluid -->
          </div>
          <div class="modal fade" id="showmodel">
            <div class="modal-dialog">
              <div class="modal-content bg-light">
                <div class="modal-header">
                  <h4 class="modal-title">Profile View</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body justify-content-center">
                  <div class="row">
                    <div class="col">
                      <table class="table">
                        
                        <tbody>
                          <tr>
                            <th scope="row">Name</th>
                            <td>:</td>
                            <td id="show_name">Mark</td>
                          </tr>
                          <tr>
                            <th scope="row">Email</th>
                            <td>:</td>
                            <td id="show_email">Jagdish@v.com</td>
                          </tr>
                          <tr>
                            <th scope="row">Mobile</th>
                            <td>:</td>
                            <td id="show_mobile">9098989080</td>
                          </tr>
                          <tr>
                            <th scope="row">Restaurant</th>
                            <td>:</td>
                            <td id="show_r_id"></td>
                          </tr>
                          <tr>
                            <th scope="row">User Type</th>
                            <td>:</td>
                            <td id="show_type">Admin</td>
                          </tr>
                          <tr>
                            <th scope="row">Registration Date</th>
                            <td>:</td>
                            <td id="show_date">200-10-02 00:00:00</td>
                          </tr>
                          
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          <!-- /.content -->
          <!-- model for filters -->