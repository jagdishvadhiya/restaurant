

      <!-- check is add new or edit form -->
<?php
  if( isset( $userData ) && !empty( $userData ) ){
      $cardTitle = "Edit ";
      $formId = 'editForm';
      $user     = $userData[0];
      $userName = $user[ 'um_name' ];
      $userEmail     = $user[ 'um_email' ];
      $userMobile    = $user[ 'um_mobile' ];
  }
?> 
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Dashborad</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php admin_c( 'dashboard' ); ?>">Dashborad</a></li>
            <li class="breadcrumb-item"><a href="<?php admin_c( 'user/user' ); ?>">User</a></li>
            <li class="breadcrumb-item"> <?php if( isset( $cardTitle ) ){echo $cardTitle;}else{ echo 'Add New'; } ?> User</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <!-- jquery validation -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                    <?php if( isset( $cardTitle ) ){echo $cardTitle;}else{ echo 'Add New'; } ?>
                    <small>
                    User
                    </small>
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form role="form" id="userForm" action="<?php user_v( 'user/formAction' ); ?>" method="post">
                    
                    <div class="card-body">

                      <?php
                      if( isset( $user ) )
                      {
                      echo '<input type="hidden" name="userId" id="userId" value="'.$user[ 'um_id' ].'">';
                      echo '<input type="hidden" name="userRestaurantId" id="userRestaurantId" value="'.$user[ 'um_rd_id' ].'">';
                      }
                      ?>
                      <div class="form-group">
                        <label for="userName">Name</label>
                        <input type="text" name="userName" class="form-control " id="userName" placeholder="Enter User Name" value="<?php if( isset( $userName ) ){echo $userName;} ?>">
                        
                      </div>
                      
                      <div class="form-group">
                        <label for="userEmail">Email</label>
                        <input type="email" name="userEmail" class="form-control " id="userEmail" placeholder="Enter user Email"value="<?php if( isset( $userEmail ) ){echo $userEmail;} ?>">
                        
                      </div>
                      
                      <div class="form-group">
                        <label for="userMobile">Mobile</label>
                        <input type="number" name="userMobile" class="form-control " id="userMobile" placeholder="Enter user Mobile"value="<?php if( isset( $userMobile ) ){echo $userMobile;} ?>">
                        
                      </div>
                      <div class="form-group">
                        <label for="userPassword">Password</label>
                        <input type="password" name="userPassword" class="form-control " id="userPassword" placeholder="Enter Password" >
                      </div>
                      <div class="form-group">
                        <label for="ConfirmPassword">Confirm Password</label>
                        <input type="password" name="confirmPassword" class="form-control " id="confirmPassword" placeholder="Confirm Password" >
                      </div>
                      <div class="form-group">
                        <label for="userType">User Type</label>
                        <select name="userType" id="userType" class="form-control .user">
                          <option value="">Select User Type</option>
                           <option value="admin"<?php if(isset($user) &&  $user[ 'um_type' ] == 'admin' ){ echo ' selected';} ?>>Admin</option>
                          <option value="user"<?php if( isset($user) && $user[ 'um_type' ] == 'user' ){ echo ' selected';} ?> >User</option>
                         
                        </select>
                      </div>
                      <?php if( getUser_s() == 'superadmin' ){ ?>
                      <div class="form-group ">
                        <label for="userRestaurant">User Restaurant</label>
                        <select name="userRestaurant" id="userRestaurant" class="form-control">
                          <option value="">Select restaurant Type</option>
                          
                        </select>
                      </div>
                    <?php }else{  ?>
                      <input type="hidden" name="userRestaurant" value="<?php echo getRestaurantId_s(); ?>">
                    <?php } ?>
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="formSubmit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
            </div><!-- /.container-fluid -->
          </div>

         </div> <!-- /.content -->
          <!-- load footer View