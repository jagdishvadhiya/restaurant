<!-- load header View -->
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Orders</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php admin_c( 'dashboard' ); ?>">Dashborad</a></li>
            <li class="breadcrumb-item">Orders</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-4">
                        <h3 class="card-title">Order List</h3>
                        <div class="dropdown">
                          <button class="btn btn-success py-0 float-right dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-file-download" ></i></button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="someOtherDiv">
                          </div>
                        </div>
                      </div>
                      <div class="col-8">
                        
                        <form action="" method="post" id="filterForm">
                          <div class="row ">
                            <div class="col-4 float-right">
                              <?php if( getUser_s() == 'superadmin' ){ ?>
                              <select name="selectRestaurant" id="selectRestaurant" class="form-control">
                                <option value="">Select Restaurant</option>
                              </select>
                              <?php } ?>
                            </div>
                            <div class="col-3 float-right">
                              <input type="text" name="selectdate" class="form-control" id="datepicker" placeholder="Select Date">
                            </div>
                            <div class="col-3 float-right">
                              <select name="selectStatus" id="selectStatus" class="form-control">
                                <option value="">Select Status</option>
                                
                                <option value="running">Running</option>
                                <option value="finish">Finished</option>
                              </select>
                            </div>
                            <div class="col-2 ">
                              <button type="submit" class="btn btn-primary "><i class="fa fa-filter" aria-hidden="true"></i>
                              </button>
                              <button type="reset" class="btn btn-primary   "  id='clearFilter'><i class="fa fa-times" aria-hidden="true"></i>
                              </button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table id="ordertable"  class=" table table-bordered table-hover table-responsive" style="width:100%">
                      <thead>
                        <tr>
                          <th>Id</th>
                          <th>Updated Date</th>
                          <th>Table name</th>
                          <th>Customer Name</th>
                          <th>Mobile</th>
                          <th>Total Item</th>
                          <th>Total Amount</th>
                          <th>Restaurant Name</th>
                          <th>Order Status</th>
                          <th>User Name</th>
                          <th>Register date</th>
                          <th>action</th>
                        </tr>
                      </thead>
                      
                      <tfoot>
                      <tr>
                        <th>Id</th>
                        <th>Date</th>
                        <th>Table name</th>
                        <th>Customer Name</th>
                        <th>Mobile</th>
                        <th>Total Item</th>
                        <th>Total Amount</th>
                        <th>Restaurant Name</th>
                        <th>Order Status</th>
                        <th>User Name</th>
                        <th>Register date</th>
                        <th>action</th>
                      </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.col -->
            </div>
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content -->
          <!-- load footer View -->
          <div class="modal fade" id="showmodel">
            <div class="modal-dialog">
              <div class="modal-content bg-light">
                <div class="modal-header">
                  <h4 class="modal-title">Restaurant View</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body justify-content-center">
                  <div class="card-body">
                    <div class="card-header p-2">
                      <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" href="#order" data-toggle="tab">Order</a></li>
                        <li class="nav-item"><a class="nav-link " href="#item" data-toggle="tab">Item</a></li>
                      </ul>
                      </div><!-- /.card-header -->
                      <div class="card-body">
                        <div class="tab-content">
                          
                          
                          
                          <div class="tab-pane active" id="order">
                            <div class="row">
                              <div class="col">
                                <table class="table">
                                  
                                  <tbody>
                                    <tr>
                                      <th scope="row">Customer Name</th>
                                      <td>:</td>
                                      <td id="show_c_name">Jk Restaurant</td>
                                    </tr>
                                    <tr>
                                      <th scope="row">Customer Mobile Number</th>
                                      <td>:</td>
                                      <td id="show_c_mobile">Jagdish Vadhiya</td>
                                    </tr>
                                    <tr>
                                      <th scope="row">Restaurant Name</th>
                                      <td>:</td>
                                      <td id="show_r_name">Jk@jk.com</td>
                                    </tr>
                                    <tr>
                                      <th scope="row">Total Items</th>
                                      <td>:</td>
                                      <td id="show_o_items">89809890980</td>
                                    </tr>
                                    <tr>
                                      <th scope="row">Total Amout</th>
                                      <td>:</td>
                                      <td id="show_o_amout">Rajkot, kothariya</td>
                                    </tr>
                                    <tr>
                                      <th scope="row" >Order Status</th>
                                      <td>:</td>
                                      <td id="show_o_status">200-10-02 00:00:00</td>
                                    </tr>
                                    <tr>
                                      <th scope="row" >Order By </th>
                                      <td>:</td>
                                      <td id="show_o_user">200-10-02 00:00:00</td>
                                    </tr>
                                    <tr>
                                      <th scope="row" >Registration Date</th>
                                      <td>:</td>
                                      <td id="show_o_date">200-10-02 00:00:00</td>
                                    </tr>
                                    
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                          <div class="tab-pane " id="item">
                            <table class="table">
                              <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Price</th>
                                  <th>Quentity</th>
                                  <th>Total</th>
                                </tr>
                              </thead>
                              <tbody id="itemTab">
                                
                                
                              </tbody>
                            </table>
                          </div>
                          
                          <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-dark" data-dismiss="modal">Close</button>
                  </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.content -->
            <!-- model for filters -->