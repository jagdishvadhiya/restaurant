<!-- check is add new or edit form --> 
<?php
if( isset( $orderData ) && !empty( $orderData ) ){
$cardTitle = "Edit ";
$formId = 'editForm';
$order     = $orderData[0];
$orderId = $order[ 'om_id' ];
$customerName = $order[ 'om_customer_name' ];
$customerMobileNumber = $order[ 'om_mobile' ];
$orderUserId = $order[ 'om_user_id' ];
$orderRestaurantId = $order[ 'om_rd_id' ];
$orderTableId = $order[ 'om_user_id' ];
$orderStatus = $order['om_status'];
}
?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Dashborad</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-order"><a href="<?php admin_c( 'dashboard' ); ?>">Dashborad </a> /</li>
            <li class="breadcrumb-item"><a href="<?php admin_c( 'order/order' ); ?>"> &nbsp; order </a> /</li>
            <li class="breadcrumb-order"> <?php if( isset( $cardTitle ) ){echo $cardTitle;}else{ echo ' Add New'; } ?> Order</li>
          </ol>
          </div><!-- /.col -->
          </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <!-- jquery validation -->
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">
                    <?php if( isset( $cardTitle ) ){echo $cardTitle;}else{ echo 'Add New'; } ?>
                    <small>
                    order
                    </small>
                    </h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form role="form" id="orderForm" action="<?php order_v( 'order/formAction' ); ?>" method="post">
                    
                    <div class="card-body">
                      <?php
                      if( isset( $order ) )
                      {
                      echo '<input type="hidden" name="orderId" id="orderId" value="'.$order[ 'om_id' ].'">';
                      echo '<input type="hidden" name="orderRestaurantId" id="orderRestaurantId" value="'.$orderRestaurantId.'">';
                       echo '<input type="hidden" name="orderUserId" id="orderUserId" value="'.$orderUserId.'">';
                       echo '<input type="hidden" name="orderTableId" id="orderTableId" value="'.$orderTableId.'">';
                       echo '<input type="hidden" name="orderStatus" id="orderStatus" value="'.$orderStatus.'">';

                      }
                      ?>
                      <div class="row">
                        <div class="col-6">
                          <div class="form-group">
                            <label for="customerName">Customer Name</label>
                            <input type="text" name="customerName" class="form-control " id="customerName" placeholder="Enter Customer Name" value="<?php if( isset( $customerName ) ){echo $customerName;} ?>">
                            
                          </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group">
                            <label for="customerNumber">Customer Mobile Number</label>
                            <input type="text" name="customerNumber" class="form-control " id="customerNumber" placeholder="Enter Customer Mobile Number" value="<?php if( isset( $customerMobileNumber ) ){echo $customerMobileNumber;} ?>">
                            
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col">
                          <div class="form-group ">
                            <label for="selectRestaurant">Restaurant</label>
                            <select name="selectRestaurant" id="selectRestaurant" class="form-control">
                              <option value="">Select restaurant </option>
                              
                            </select>
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group ">
                            <label for="selectUser">User</label>
                            <select name="selectUser" id="selectUser" class="form-control">
                              <option value="">Select User</option>
                              
                            </select>
                          </div>
                        </div>
                         <div class="col">
                          <div class="form-group ">
                            <label for="selectTable">Table</label>
                            <select name="selectTable" id="selectTable" class="form-control">
                              <option value="">Select Table</option>
                              
                            </select>
                          </div>
                        </div>
                         <div class="col">
                          <div class="form-group ">
                            <label for="selectStatus">Status</label>
                            <select name="selectStatus" id="selectStatus" class="form-control">
                              <option value="">Select Status</option>
                              <option value="running">Running</option>
                              <option value="finish">Finish</option>
                           
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header">
                          Items
                        </div>
                        <div class="card-block cardDiv">
                          
                          <div class="row m-2 itemDiv">
                            <div class="col-3">
                              <div class="form-group">
                                <label for="orderPrice">Select Item</label>
                                <select name="selectItemMenu[]" id="selectItemMenu" class="form-control">
                                  <option value="">Select Menu</option>
                                  
                                </select>
                                
                              </div>
                            </div>
                            <div class="col-3">
                              <div class="form-group">
                                <label for="itemPrice">Item Price</label>
                                <input type="number" name="itemPrice[]" class="form-control " id="itemPrice"  value="<?php if( isset( $itemAmout ) ){echo $itemAmout;}else{echo '0';} ?>" min='0' disabled>
                                
                              </div>
                            </div>
                            <div class="col-2">
                              <div class="form-group">
                                <label for="orderQty">Quentity</label>
                                <input type="number" name="orderQty[]" class="form-control " id="orderQty" placeholder="Enter order Quentity" value="<?php if( isset( $orderQty ) ){echo $orderQty;}else{echo '1';} ?>" min='1' >
                                
                              </div>
                            </div>
                            <div class="col-2">
                              <div class="form-group">
                                <label for="orderAmout">Total Amout</label>
                                <input type="text" name="totalAmout[]" class="form-control " id="totalAmout" value="<?php if( isset( $orderTotalAmout ) ){echo $orderTotalAmout;}else{echo "0";} ?>" disabled>
                              </div>
                              
                            </div>
                            <div class="col-2">
                              <div class="form-group">
                                <center><label for="orderAmout">Add More</label></center>
                                
                                <center>  <button type="button" class="btn btn-primary my-0 py-0" id="AddNewItem">+</button></center>
                              </div>
                            </div>
                          </div>
                          <div class="row mx-3">
                            <div class="col-12">
                              <table class="table table-bordered" id="itemdetails">
                                <thead>
                                  <tr>
                                    <th>Item</th>
                                    <th>Price(Per Pice)</th>
                                    <th>Quentity</th>
                                    <th>Total Amount</th>
                                    <th>Remove</th>
                                  </tr>
                                </thead>
                                <tbody  id="itemdetailsbody">
                                  
                                  
                                </tbody>
                                <tfoot class="" id="tfooter">
                                <tr>
                                  <th></th>
                                  <th></th>
                                  <th>Total Order Amout</th>
                                  <th id="totalPriceAmout">000</th>
                                  <th></th>
                                 
                                </tr>
                                </tfoot>
                              </table>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                      <div class="form-group ">
                        <!-- <label for="selectRestaurant">User Name</label> -->
                       
                        <input type="hidden" name="orderId" id="orderOrderId" value="<?php if( isset( $orderOrderId ) ){echo $orderOrderId;}else{echo "0";} ?>">
                        <!-- <input type="text" name="UserName" id="orderOrderUserName" value="a" class="form-control" disabled> -->
                      </div>
                      
                      <!-- /.card-body -->
                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="formSubmit">Submit</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
            </div><!-- /.container-fluid -->
          </div>
          </div> <!-- /.content -->
          <!-- load footer View -->
          <!--
          <form>
            <input type="checkbox" id="pizza" name="pizza" value="yes">
            <label for="pizza">I would like to order a</label>
            <select id="pizza_kind" name="pizza_kind">
              <option>(choose one)</option>
              <option value="margaritha">Margaritha</option>
              <option value="hawai">Hawai</option>
            </select>
            pizza.
          </form>
          <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
          <script>
          var update_pizza = function () {
          if ($("#pizza").is(":checked")) {
          $('#pizza_kind').prop('disabled', false);
          }
          else {
          $('#pizza_kind').prop('disabled', 'disabled');
          }
          };
          $(update_pizza);
          $("#pizza").change(update_pizza);
          </script>