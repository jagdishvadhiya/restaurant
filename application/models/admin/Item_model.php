<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function getItem( $id = "" )
	{
		$this->db->select( "*" ); 
		if( $id != "" )
		{
			$this->db->where( "im_id", $id );
		}
		$result = $this->db->get( "item_master" );
		if( $result->num_rows() > 0 )
		{
			return $result->result_array();
		}else{
			return false;
		}
	}
	// Add or Update Item
	public function actionItem( $dataArray )
	{
		if(!empty($dataArray))
		{
			if( !isset( $dataArray['im_id'] ) )
			{
				//insert code
				$insert_data = $dataArray;
				$insert_data['im_inserted_date'] = date("Y-m-d H:i:m");
				if($this->db->insert("item_master",$insert_data))
				{
						return true;
				}else{
						return false;
				}
				
			}else{
					//update code
					$item_id = $dataArray['im_id'];
					$updateData = $dataArray;

					unset( $updateData['im_id'] );
					$this->db->where( 'im_id', $item_id );
					if($this->db->update("item_master",$updateData))
					{
							return true;
					}else{
							return false;
					}
			}
		
		}
	}


	// delete Item by id
	public function deleteItemById( $id = "" )
	{
		if( $id != "" )
		{
			$item_data  = $this->db->select( 'im_order_id' )->where( 'im_id' , $id )->get( 'item_master' )->result_array();
			$order_id = $item_data[0]['im_order_id'];
			$this->db->trans_begin();
			$order_data = $this->db->select( 'om_total_item' )->where( 'om_id' , $order_id )->get( 'order_master' )->result_array();
			if( $order_data[0]['om_total_item']-1 == 0 )
			{
				$this->db->where( 'om_id' , $order_id )->delete( 'order_master' );
				$this->db->where( 'im_id' , $id );
				$this->db->delete('item_master');

			}else{
				$update_qty['om_total_item'] = $order_data[0]["om_total_item"]-1 ;
				$this->db->where( 'om_id' , $order_id )->update( 'order_master', $update_qty );
				$this->db->where( 'im_id' , $id );
				$this->db->delete('item_master');
			}
			if ($this->db->trans_status() === FALSE)
			{
			        $this->db->trans_rollback();
			        return false;
			}
			else
			{
			        $this->db->trans_commit();
			        return true;
			}
		 }else{
			return false;
		}
		
	}

	public function getItemsByOrderId( $order_id = "" )
	{
		$this->db->select( "*" ); 
		if( $order_id != "" )
		{
			$this->db->where( "im_order_id", $order_id );
		}
		$result = $this->db->get( "item_master" );
		if( $result->num_rows() > 0 )
		{
			return $result->result_array();
		}else{
			return false;
		}
	}
	public function getMenuNameIdByMenuIdRes( $menu_id = "" )
	{
		$data = $this->db->select( 'menu_rd_id' )->where( "menu_id", $menu_id )->get( 'menu_master' )->result_array();    
		$restaurant_id = $data[0]['menu_rd_id'];
		$result = $this->db->select( 'menu_id,menu_name' )->where( "menu_rd_id", $restaurant_id )->get( 'menu_master' );    
		if( $result->num_rows() > 0 )
		{
			return $result->result_array();
		}else{
			return false;
		}
	}
	

}

/* End of file Item_model.php */
/* Location: ./application/models/admin/Item_model.php */