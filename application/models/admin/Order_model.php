<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function getOrder( $id = "" )
	{
		$this->db->select( "*" ); 
		if( $id != "" )
		{
			$this->db->where( "om_id", $id );
		}
		$result = $this->db->get( "order_master" );
		if( $result->num_rows() > 0 )
		{
			return $result->result_array();
		}else{
			return false;
		}
	}
	public function getUserNameByOrderId( $id = "" )
	{
		$this->db->select( "om_id,om_user_name" ); 
		if( $id != "" )
		{
			$this->db->where( "om_id", $id );
		}
		$result = $this->db->get( "order_master" );
		if( $result->num_rows() > 0 )
		{
			$data = $result->result_array();
			return $data[0];
		}else{
			return false;
		}
	}
	public function getUserBYRestaurantId( $id = "" )
	{
		$this->db->select( "um_id,um_name" ); 
		if( $id != "" )
		{
			$this->db->where( "um_rd_id", $id );
		}
		$result = $this->db->get( "user_master" );
		if( $result->num_rows() > 0 )
		{
			$data = $result->result_array();
			return $data;
		}else{
			return false;
		}
	}
	public function getOrderIdBYRestaurantId( $id = "" )
	{
		
       
		$this->db->select( "om_id" ); 
		if( $id != "" )
		{
			$this->db->where( "om_rd_id", $id );
		}
		$result = $this->db->get( "order_master" );
		if( $result->num_rows() > 0 )
		{
			$data = $result->result_array();
			$da = array();
			foreach ($data as $key => $value) {
				# code...
				$da[] = $value['om_id'];
			}
			return $da;
		}else{
			return false;
		}
	}
	public function getMenuIdByItemName( $item_name = "", $item_price = "" )
	{
		$this->db->select( 'menu_id' );
		$this->db->where( 'menu_name', $item_name );
		$this->db->where( 'menu_price', $item_price );
		$data = $this->db->get( "menu_master" );
		if( $data->num_rows() > 0 )
		{
			$data = $data->result_array();
			return $data[0]['menu_id'];
		}
	}
	public function orderAction( $dataArray )
	{
		if( !empty( $dataArray ) )
		{
			if( isset( $dataArray['om_id'] ) )
			{
				$order_id = $dataArray['om_id'];
				$updateData = $dataArray;
				unset( $updateData['om_id'] );
				unset( $updateData['items'] );
				$this->db->where( 'om_id', $order_id );
					if($this->db->update("order_master",$updateData))
					{
						
							
							$items = $dataArray['items'];
							$itemArray = array();
							foreach ($items as $key => $value) {
								# code...
								$oneItem = array(
									'im_name'    => $value['itemName'],
									'im_item_id' => $this->getMenuIdByItemName( $value['itemName'], $value['itemPrice'] ),
									'im_qty'     => $value['itemQty'],
									'im_amout'   => $value['itemPrice'],
									'im_total_amout'=>$value['itemQty'] * $value['itemPrice'],
									'im_order_id' => $order_id,
									'im_inserted_date'=>date("Y-m-d H:i:m")
								);
								$itemArray[] = $oneItem;
							}
							if( $this->db->where( 'im_order_id', $order_id )->delete( "item_master" ) )
							{
									if($this->db->insert_batch('item_master', $itemArray))
									{

										return true;
									} 
							}
							
							
						}else{
								return false;
						}


			}else{

				$insert_data = $dataArray;
				unset( $insert_data['items'] );
				$insert_data['om_inserted_date'] = date("Y-m-d H:i:m");
				// print_r( $insert_data  );
				if($this->db->insert("order_master",$insert_data))
				{
					$order_id = $this->db->insert_id();
					$items = $dataArray['items'];
					$itemArray = array();
					foreach ($items as $key => $value) {
						# code...
						$oneItem = array(
							'im_name'    => $value['itemName'],
							'im_item_id' => $value['itemItemId'],
							'im_qty'     => $value['itemQty'],
							'im_amout'   => $value['itemPrice'],
							'im_total_amout'=>$value['itemQty'] * $value['itemPrice'],
							'im_order_id' => $order_id,
							'im_inserted_date'=>date("Y-m-d H:i:m")
						);
						$itemArray[] = $oneItem;
					}
					if($this->db->insert_batch('item_master', $itemArray))
					{

						return true;
					} 
				}else{
						return false;
				}
			}
		}
	}


	// delete Order by id
	public function deleteOrderById( $id = "" )
	{
		if( $id != "" )
		{
			$this->db->where( 'om_id' , $id );
			$this->db->delete('order_master');
			$this->db->where( 'im_order_id' , $id );
			$this->db->delete('item_master');
			return true;
		}else{
			return false;
		}
	}



}

/* End of file Order_model.php */
/* Location: ./application/models/admin/Order_model.php */