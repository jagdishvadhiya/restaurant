<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function login( $dataArray )
	{
		if( !empty( $dataArray ) )
		{
			
			$result = $this->db->select( "*" )->where( $dataArray )->get( "user_master" );
			if( $result->num_rows() > 0 )
			{
				return $result->result_array();
			}else{
				return false;
			}

		}else{
			return false;

		}
	}
	

}

/* End of file Login_model.php */
/* Location: ./application/models/admin/Login_model.php */