<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('admin/Genral_model');
	}
	public function getUser( $id = "" )
	{
		$this->db->select( "*" ); 
		if( $id != "" )
		{
			$this->db->where( "um_id", $id );
		}
		$result = $this->db->get( "user_master" );
		if( $result->num_rows() > 0 )
		{
			return $result->result_array();
		}else{
			return false;
		}
	}
	// delete user by id
	public function deleteUserById( $id = "" )
	{
		if( $id != "" )
		{
			$this->db->where( 'um_id' , $id );
			$this->db->delete('user_master');
			$this->db->where( 'user_id' , $id );
			$this->db->delete('role_master');
			return true;
		}else{
			return false;
		}
	}
	
	// Add or Update User
	public function actionUser( $dataArray )
	{
		if(!empty($dataArray))
		{
			if( !isset( $dataArray['um_id'] ) )
			{
				//insert code
				$insert_data = $dataArray;
				$insert_data['um_inserted_date'] = date("Y-m-d H:i:m");
				if($this->db->insert("user_master",$insert_data))
				{
					$userInsertid = $this->db->insert_id();
					$restaurantInsertId = $dataArray[ 'um_rd_id' ];
						$roleData = array(
							'restaurant_id' => $restaurantInsertId,
							'user_id'      => $userInsertid,
							'admin_is'     => $dataArray[ 'um_type' ]
							
						);
						if( $dataArray[ 'um_type' ] != 'admin' )
						{
							$roleData[ 'p_delete' ] = '0';
							$roleData[ 'p_edit' ] = '0';
						}else{
							$roleData[ 'p_delete' ] = '1';
							$roleData[ 'p_edit' ] = '1';
						}
						if( $this->db->insert( "role_master" , $roleData ) )
						{

							return true;
						}
				}else{
						return false;
				}
				
			}else{
					//update code
					$user_id = $dataArray['um_id'];
					$updateData = $dataArray;
					unset( $updateData['um_id'] );
					$this->db->where( 'um_id', $user_id );
					if($this->db->update("user_master",$updateData))
					{
						$checkRole = $this->Genral_model->getRoleBYUserId( $user_id );
						if( $dataArray['um_type'] == $checkRole )
						{
							return true;
						}else{
								$updateData = [
								   'admin_is' => $dataArray['um_type'],
								];
								if( $dataArray[ 'um_type' ] != 'admin' )
									{
										$updateData[ 'p_delete' ] = '0';
										$updateData[ 'p_edit' ] = '0';
									}else{
										$updateData[ 'p_delete' ] = '1';
										$updateData[ 'p_edit' ] = '1';
									}
								
								$this->db->where( 'user_id', $user_id );
								$this->db->update( 'role_master', $updateData ); 
								return true;
						}
					
					}else{
							return false;
					}
			}
		
		}
	}

	public function getUserBYRestaurantId( $id = "" )
	{
		$this->db->select( "um_id,um_name" ); 
		if( $id != "" )
		{
			$this->db->where( "um_rd_id", $id );
		}
		$result = $this->db->get( "user_master" );
		if( $result->num_rows() > 0 )
		{
			$data = $result->result_array();
			return $data;
		}else{
			return false;
		}
	}
	public function getAllUserBYRestaurantId( $id = "" )
	{
		$this->db->select( "*" ); 
		if( $id != "" )
		{
			if( $id != "0" )
			{

			$this->db->where( "um_rd_id", $id );
			}
		}
		$result = $this->db->get( "user_master" );
		if( $result->num_rows() > 0 )
		{
			$data = $result->result_array();
			return $data;
		}else{
			return false;
		}
	}
	

}

/* End of file user_model.php */
/* Location: ./application/models/admin/user_model.php */