<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Genral_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		
	}
	public function getCountByTable( $table = "" )
	{
		if( $table != "" ) 
		{
			if( getUser_s() != 'superadmin' )
			{
				$restaurantId = getRestaurantId_s();
				if( $table == 'user_master' )
					$this->db->where( 'um_rd_id', $restaurantId );
				if( $table == 'menu_master' )
					$this->db->where( 'menu_rd_id', $restaurantId );
				if( $table == 'table_master' )
					$this->db->where( 'tm_rd_id', $restaurantId );
				if( $table == 'order_master' )
					$this->db->where( 'om_rd_id', $restaurantId );
				if( $table == 'item_master' )
				{
					$this->load->model('admin/Order_model');
		            $data = $this->Order_model->getOrderIdBYRestaurantId( $restaurantId );
		            $this->db->where_in( 'im_order_id', $data );
				}


				
			}
			$result = $this->db->select('*')->get( $table );
			
			return $result->num_rows();
		}	 
	}
	public function getRoleBYUserId( $user_id = '' )
	{
		if( $user_id != "" )
		{
			$this->db->select( 'admin_is' );
			$this->db->where( "user_id", $user_id );
			$data = $this->db->get( 'role_master' )->result_array();
			return $data[0]['admin_is'];
		}
	}
	// update primition of user
	public function updatePermition( $user_id = "", $action = "", $value = "" )
	{
		if( $user_id != "" && $action != "" && $value != "" )
		{
			$this->db->where( 'user_id', $user_id );

			if( $action == 'delete' )
			{
				$update = array( 'p_delete' => $value );
				$this->db->update( 'role_master', $update);
				return true;
			}
			if( $action == 'edit' )
			{
				$update = array( 'p_edit' => $value );
				$this->db->update( 'role_master',$update );
				return true;
			}

		}else{
			return false;
		}
	}

}

/* End of file Genral_model.php */
/* Location: ./application/models/admin/Genral_model.php */