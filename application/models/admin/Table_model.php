<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Table_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function getTable( $id = "" )
	{
		$this->db->select( "*" ); 
		if( $id != "" )
		{
			$this->db->where( "tm_id", $id );
		}
		$result = $this->db->get( "table_master" );
		if( $result->num_rows() > 0 )
		{
			return $result->result_array();
		}else{
			return false;
		}
	}
	// Add or Update Menu
	public function actionTable( $dataArray )
	{
		if(!empty($dataArray))
		{
			if( !isset( $dataArray['tm_id'] ) )
			{
				//insert code
				$insert_data = $dataArray;
				$insert_data['inserted_date'] = date("Y-m-d H:i:m");
				if($this->db->insert("table_master",$insert_data))
				{
						return true;
				}else{
						return false;
				}
				
			}else{
					//update code
					$table_id = $dataArray['tm_id'];
					$updateData = $dataArray;
					unset( $updateData['tm_id'] );
					$this->db->where( 'tm_id', $table_id );
					if($this->db->update("table_master",$updateData))
					{
							return true;
					}else{
							return false;
					}
			}
		
		}
	}

	// delete Table by id
	public function deleteTableById( $id = "" )
	{
		if( $id != "" )
		{
			$this->db->where( 'tm_id' , $id );
			$this->db->delete('table_master');
			return true;
		}else{
			return false;
		}
	}

	public function getTableBYRestaurantId( $id = "" )
	{
		$this->db->select( "tm_id,tm_name" ); 
		if( $id != "" )
		{
			$this->db->where( "tm_rd_id", $id );
		}
		$result = $this->db->get( "table_master" );
		if( $result->num_rows() > 0 )
		{
			$data = $result->result_array();
			return $data;
		}else{
			return false;
		}
	}
}

/* End of file Menu_model.php */
/* Location: ./application/models/admin/Menu_model.php */