<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function getMenu( $id = "" )
	{
		$this->db->select( "*" ); 
		if( $id != "" )
		{
			$this->db->where( "menu_id", $id );
		}
		$result = $this->db->get( "menu_master" );
		if( $result->num_rows() > 0 )
		{
			return $result->result_array();
		}else{
			return false;
		}
	}
	public function getMenuPriceBYId( $Menu_id = "" )
	{
		$this->db->select( "menu_price" ); 
		if( $Menu_id != "" )
		{
			$this->db->where( "menu_id", $Menu_id );
		}
		$result = $this->db->get( "menu_master" );
		if( $result->num_rows() > 0 )
		{
			$data = $result->result_array();
			return $data[0]['menu_price'];
		}else{
			return false;
		}
	}
	public function getMenuNameById( $Menu_id = "" )
	{
		$this->db->select( "menu_name" ); 
		if( $Menu_id != "" )
		{
			$this->db->where( "menu_id", $Menu_id );
		}
		$result = $this->db->get( "menu_master" );
		if( $result->num_rows() > 0 )
		{
			$data = $result->result_array();
			return $data[0]['menu_name'];
		}else{
			return false;
		}
	}
	public function getMenuNameId( $Menu_id = "", $restaurantId = "" )
	{
		$this->db->select( "menu_id,menu_name" ); 
		
		if( $Menu_id != "" )
		{
			$this->db->where( "menu_id", $Menu_id );
		}
		if( $restaurantId != "" )
		{
			$this->db->where( "menu_rd_id", $restaurantId );
		}
		$result = $this->db->get( "menu_master" );
		if( $result->num_rows() > 0 )
		{
			$data = $result->result_array();
			return $data;
		}else{
			return false;
		}
	}

	// Add or Update Menu
	public function actionMenu( $dataArray )
	{
		if(!empty($dataArray))
		{
			if( !isset( $dataArray['menu_id'] ) )
			{
				//insert code
				$insert_data = $dataArray;
				$insert_data['menu_inserted'] = date("Y-m-d H:i:m");
				if($this->db->insert("menu_master",$insert_data))
				{
						return true;
				}else{
						return false;
				}
				
			}else{
					//update code
					$menu_id = $dataArray['menu_id'];
					$updateData = $dataArray;
					unset( $updateData['menu_id'] );
					$this->db->where( 'menu_id', $menu_id );
					if($this->db->update("menu_master",$updateData))
					{
							return true;
					}else{
							return false;
					}
			}
		
		}
	}

	// delete Menu by id
	public function deleteMenuById( $id = "" )
	{
		if( $id != "" )
		{
			$this->db->where( 'menu_id' , $id );
			$this->db->delete('menu_master');
			return true;
		}else{
			return false;
		}
	}
}

/* End of file Menu_model.php */
/* Location: ./application/models/admin/Menu_model.php */