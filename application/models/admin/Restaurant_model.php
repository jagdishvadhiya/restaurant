<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Restaurant_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function getRestaurant( $id = "" )
	{
		$this->db->select( "*" );
		if( $id != "" )
		{
			$this->db->where( "rd_id", $id );
		}
		$result = $this->db->get( "restaurant_detail" );
		if( $result->num_rows() > 0 )
		{
			return $result->result_array();
		}else{
			return false;
		}
	}
	// get restaurant name
	public function getRestaurantName( $id = "" )
	{
		$this->db->select( "rd_restaurant_name" );
		if( $id != "" )
		{
			$this->db->where( "rd_id", $id );
		}
		$result = $this->db->get( "restaurant_detail" );
		if( $result->num_rows() > 0 )
		{
			$data = $result->result_array();
			return $data[0]['rd_restaurant_name'];
		}else{
			return false;
		}
	}
	// get restaurant name and id
	public function getRestaurantNameId()
	{
		$this->db->select( "rd_id,rd_restaurant_name" );
		if( getUser_s() != 'superadmin' )
		{
			$resId = getRestaurantId_s();
			$this->db->where( 'rd_id =', $resId );

		}
		if( getUser_s() == 'superadmin' )
		{
			$resId = getRestaurantId_s();
			$this->db->where( 'rd_id !=', 0 );

		}
		
		
		$result = $this->db->get( "restaurant_detail" );
		if( $result->num_rows() > 0 )
		{
			$data = $result->result_array();
			return $data;
		}else{
			return false;
		}
	}
	// add new restaurant
	public function actionRestaurant( $dataArray )
	{
		if(!empty($dataArray))
		{
			if( !isset( $dataArray['rd_id'] ) )
			{
				//insert code
				$insert_data = $dataArray;
				unset( $insert_data['um_password'] );
				
				$insert_data['rd_inserted_date'] = date("Y-m-d H:i:m");
				if($this->db->insert("restaurant_detail",$insert_data))
				{
					$restaurantInsertId = $this->db->insert_id();
					$user_insert = array(
						'um_name'           => $dataArray[ 'rd_restaurant_owner_name' ],
						'um_email'          => $dataArray[ 'rd_restaurant_email' ],
						'um_mobile'         => $dataArray[ 'rd_mobile_number' ],
						'um_password'       => md5( $dataArray[ 'um_password' ] ),
						'um_type'			=> 'admin',
						'um_rd_id'			=> $this->db->insert_id(),
						'um_inserted_date'  => date( "Y-m-d H:i:m" )

					);
					if( $this->db->insert("user_master",$user_insert) )
					{
						$userInsertid = $this->db->insert_id();
						$roleData = array(
							'restaurant_id' => $restaurantInsertId,
							'user_id'      => $userInsertid,
							'admin_is'     => 'admin',
							'p_delete'      => '1',
							'p_edit'        => '1'
						);
						if( $this->db->insert( "role_master" , $roleData ) )
						{

							return true;
						}
					}
				}else{
						return false;
				}
				
			}else{
					//update code
					$restaurant_id = $dataArray['rd_id'];
					$updateData = $dataArray;
					if( isset( $dataArray['um_password'] ) )
					{
						unset( $updateData['um_password'] );
					}
					unset( $updateData['rd_id'] );
					$this->db->where( 'rd_id', $restaurant_id );
					if($this->db->update("restaurant_detail",$updateData))
					{
							$user_insert = array(
								'um_name'           => $dataArray[ 'rd_restaurant_owner_name' ],
								'um_email'          => $dataArray[ 'rd_restaurant_email' ],
								'um_mobile'         => $dataArray[ 'rd_mobile_number' ],
								'um_type'			=> 'admin',
								'um_rd_id'			=> $dataArray['rd_id']
							

							);
						if( isset( $dataArray['um_password'] ) )
						{
							$user_insert[ 'um_password' ] = md5( $dataArray[ 'um_password' ] );
						}
						$this->db->where( 'um_rd_id', $dataArray['rd_id'] );
						$this->db->where( 'um_email', $user_insert['um_email'] );
						if( $this->db->update("user_master",$user_insert) )
						{
							return true;
						}
					}else{
							return false;
					}
			}
		
		}
	}
	// delete restaurant
	public function deleteRestaurantById( $id = "" )
	{
		if( $id != "" )
		{
			$this->db->where( 'rd_id' , $id );
			$this->db->delete('restaurant_detail');
			$this->db->where( 'menu_rd_id', $id )->delete( "menu_master" );
			$this->db->where( 'tm_rd_id', $id )->delete( "table_master" );
			$this->db->where( 'um_rd_id', $id )->delete( "user_master" );
			$data =  $this->db->select( 'om_id' )->where( 'om_rd_id', $id )->get( "order_master" )->result_array();
			foreach ($data as $key => $value) {
				# code...
					$this->db->where( 'im_order_id', $value['om_id'] )->delete( "item_master" );
			}
			$this->db->where( 'om_rd_id', $id )->delete( "order_master" );
			
			return true;
		}else{
			return false;
		}
	}
}
/* End of file Restaurant_model.php */
/* Location: ./application/models/admin/Restaurant_model.php */