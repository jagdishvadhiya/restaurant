<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function checkEmailExist( $email = "")
	{

		if( $email != "" )
		{
			$result = $this->db->select( "*" )->where('um_email' , $email)->get( "user_master" );
			if( $result->num_rows() > 0 )
			{
				return true;
			}else{
				return false;
			}
		}else {
			return false;
		}
	}

}

/* End of file Ajax_model.php */
/* Location: ./application/models/ajax/Ajax_model.php */