<?php 
$CI =& get_instance();
function templat_url( $url = "" )
{
	$url = base_url( "assets/templat/" ).$url;
	echo $url;
}
function get_count()
{
	$CI =& get_instance();
	
}
function admin_c( $url = "" )
{
	echo base_url().'admin/'.$url;
}
function user_v( $url = "" )
{
	echo base_url().'admin/user/'.$url;
}
function restaurant_v( $url = "" )
{
	echo base_url().'admin/restaurant/'.$url;
}
function menu_v( $url = "" )
{
	echo base_url().'admin/menu/'.$url;
}
function table_v( $url = "" )
{
	echo base_url().'admin/table/'.$url;
}
function order_v( $url = "" )
{
	echo base_url().'admin/order/'.$url;
}
function item_v( $url = "" )
{
	echo base_url().'admin/item/'.$url;
}
// session helper functions
function getUser_s()
{
	$CI =& get_instance();
	return $CI->session->userdata('user_type');
}
function getUserId_s()
{
	$CI =& get_instance();
	return $CI->session->userdata('user_id');
}
function getUserName_s(  )
{

	$CI =& get_instance();
	$user_id = $CI->session->userdata('user_id');
	$CI->db->select( 'um_name' );
	$CI->db->where( 'um_id', $user_id );
	$data = $CI->db->get( 'user_master' )->result_array();
	return $data[0]['um_name'];
	// return ture;
}
function getRestaurantId_s()
{
	$CI =& get_instance();
	return $CI->session->userdata('restaurant_id');
}

function getRestaurantName_s()
{
	$CI =& get_instance();
	$restaurantId = $CI->session->userdata('restaurant_id');
	$CI->load->model( 'admin/Restaurant_model' );
	$data = $CI->Restaurant_model->getRestaurantName( $restaurantId );
    if( $data != false && $data != null)
    {
        return $data;
    }else{
      return 'Super Admin';
    }
	
}

// prmition management helper function
function getDeletePermition( $user_id )
{
	$CI =& get_instance();
	$CI->db->select( 'p_delete' );
	$CI->db->where( 'user_id', $user_id );
	$data = $CI->db->get( 'role_master' )->result_array();
	return $data[0]['p_delete'];
	// return ture;
}
function getEditPermition( $user_id )
{
	$CI =& get_instance();
	$CI->db->select( 'p_edit' );
	$CI->db->where( 'user_id', $user_id );
	$data = $CI->db->get( 'role_master' )->result_array();
	return $data[0]['p_edit'];
}
function getDeletePermitionAjax(  )
{

	$CI =& get_instance();
	$user_id = $CI->session->userdata('user_id');
	$CI->db->select( 'p_delete' );
	$CI->db->where( 'user_id', $user_id );
	$data = $CI->db->get( 'role_master' )->result_array();
	return $data[0]['p_delete'];
	// return ture;
}
function getEditPermitionAjax(  )
{
	$CI =& get_instance();
		$user_id = $CI->session->userdata('user_id');
	$CI->db->select( 'p_edit' );
	$CI->db->where( 'user_id', $user_id );
	$data = $CI->db->get( 'role_master' )->result_array();
	return $data[0]['p_edit'];
}
 ?>
