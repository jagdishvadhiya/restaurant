<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('admin/User_model');
		$this->load->model('admin/Restaurant_model');
	}
	public function index()
	{
		$profileView = array();
		
	
			$userId = getUserId_s();
			$restaurantId = getRestaurantId_s();
			$data = $this->User_model->getUser( $userId );
			$profileView['profileData'] = $data[0];
		
				$rdata = $this->Restaurant_model->getRestaurant( $restaurantId );
				
				$profileView['restaurantData'] = $rdata[0];
			

			$udata = $this->User_model->getAllUserBYRestaurantId( $restaurantId );
			$profileView['userData'] = $udata;
		  
		$viewdata['profile'] = $profileView;
		$header[ 'title' ] = "Profile | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
			$this->load->view('admin/profile/profile', $viewdata);
		$this->load->view( "admin/templat/footer", $footer );
	}
	public function profileAdminUpdate()
	{
		$response = array( 'success' => false, 'message' => array() );
		$response['success'] = true;
		$dataArray = array(
					'rd_restaurant_name'       => $this->input->post( 'restaurantName' ),
					'rd_restaurant_owner_name' => $this->input->post( 'ownerName' ),
					'rd_restaurant_email'      => $this->input->post( 'ownerEmail' ),
					'rd_mobile_number'         => $this->input->post( 'ownerMobile' ),
					'rd_address' 			   => $this->input->post( 'ownerAddress' )
					
					
				);
			if( $this->input->post( 'restaurantId' )  )
			{
				
				if( $this->input->post( 'ownerPassword' ) )
				{
					$dataArray['um_password'] = $this->input->post( 'ownerPassword' );
				}
				$dataArray['rd_id'] = $this->input->post( 'restaurantId' );
			}
			if( $this->Restaurant_model->actionRestaurant( $dataArray ) )
			{
				$response['message']['error'] = true;
			}else{
				$response['message']['error'] = false;
			}
				echo json_encode( $response );
	}

	// super admin profile update
		public function profileSuperAdminUpdate()
	{
		$response = array( 'success' => false, 'message' => array() );
		$response['success'] = true;
		$dataArray = array(
					'um_name'       => $this->input->post( 'ownerName' ),
					'um_email'      => $this->input->post( 'ownerEmail' ),
					'um_mobile'     => $this->input->post( 'ownerMobile' ),
					'um_type'       => 'superadmin'
					
					
				);
			if( $this->input->post( 'userId' )  )
			{
				
				if( $this->input->post( 'ownerPassword' ) )
				{
					$dataArray['um_password'] = md5( $this->input->post( 'ownerPassword' ) );
				}
				$dataArray['um_id'] = $this->input->post( 'userId' );
			}
			if( $this->User_model->actionUser( $dataArray ) )
			{
				$response['message']['error'] = true;
			}else{
				$response['message']['error'] = false;
			}
				echo json_encode( $response );
	}

}

/* End of file Profile.php */
/* Location: ./application/controllers/admin/Profile.php */