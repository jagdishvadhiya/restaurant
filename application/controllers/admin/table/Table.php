<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Table extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if( ! $this->session->userdata( "login" ) )
		{
			redirect( 'admin/login' ,'refresh');
		}
		$this->load->model( "admin/Table_model" );
	}
	public function index()
	{
		$tabledata = array();
		$data = $this->Table_model->getTable();
		if( $data != false )
		{
			$tabledata = $data;
		}
		$tableview[ "tabledata" ] = $tabledata;
		$header[ 'title' ] = "Table | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
		$this->load->view('admin/table/table', $tableview);	
		$this->load->view( "admin/templat/footer", $footer );
	}
	public function getTableById()
	{
		$id = $this->input->post( 'table_id' );
		$data = $this->Table_model->getTable( $id );
		$this->load->model('admin/Restaurant_model');
		$data_restaurant = $this->Restaurant_model->getRestaurantName( $data[0]['tm_rd_id'] );
		$data[0]['restaurant_name'] = $data_restaurant;
		if( $data != false )
		{
			echo json_encode( $data[0] );
		}else{
			echo 0;
		}
	}
	// form action
	public function formAction()
	{
		$response = array( 'success' => false, 'message' => array() );
		$this->form_validation->set_rules('tableName', 'Table Name', 'trim|required');
		$this->form_validation->set_rules('selectRestaurant', 'Restaurant', 'trim|required');
		$this->form_validation->set_error_delimiters( "<p class='text-danger'>","</p>" );
		if ($this->form_validation->run() == TRUE ) {
			# code...
			$response['success']  = true;
			$dataArray = array(
					'tm_name'           => $this->input->post( 'tableName' ),
					'tm_rd_id'         => $this->input->post( 'selectRestaurant' )
					
				);
			if( $this->input->post( 'tableId' ) )
			{
				$dataArray['tm_id'] = $this->input->post( 'tableId' );
			}
			if( $this->Table_model->actionTable( $dataArray ) )
			{
				$response['message']['error'] = true;
			}else{
				$response['message']['error'] = false;
			}
			
		} else {
			# code...
			foreach ($this->input->post() as $key => $value) {
				# code...
				$response['message'][$key] = form_error( $key );
			}
		}
		echo json_encode( $response );
	}
	// table form show for add new or edit
	public function tableAction( $id = "" )
	{

		$tableData = array();
		if( $id != "" )
		{
			$data = $this->Table_model->gettable( $id );
			if( $data != false )
			{
				$tableData = $data;
			}
		}
		
		$viewData['tableData'] = $tableData;
		$header[ 'title' ] = "Add Table | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
		$this->load->view('admin/table/action' , $viewData);
		$this->load->view( "admin/templat/footer", $footer );
	}
	// delete Table
	public function deleteTable()
	{
		$id = $this->input->post( 'table_id' );
		if ( $this->Table_model->deleteTableById( $id ) )
		{
			echo 1;
		}else{
			echo 0;
		}
	}

}

/* End of file Table.php */
/* Location: ./application/controllers/admin/table/Table.php */