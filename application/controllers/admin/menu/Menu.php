<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if( ! $this->session->userdata( "login" ) )
		{
			redirect( 'admin/login' ,'refresh');
		}
		$this->load->model( "admin/Menu_model" );
	}
	public function index()
	{
		$menudata = array();
		$data = $this->Menu_model->getMenu();
		if( $data != false )
		{
			$menudata = $data;
		}
		$menuview[ "menudata" ] = $menudata;
		$header[ 'title' ] = "Menu | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
		$this->load->view('admin/menu/menu', $menuview);	
		$this->load->view( "admin/templat/footer", $footer );
	}

	public function getMenuById()
	{
		$id = $this->input->post( 'menu_id' );
		$data = $this->Menu_model->getMenu( $id );
		$this->load->model('admin/Restaurant_model');
		$data_restaurant = $this->Restaurant_model->getRestaurantName( $data[0]['menu_rd_id'] );
		$data[0]['restaurant_name'] = $data_restaurant;
		if( $data != false )
		{
			echo json_encode( $data[0] );
		}else{
			echo 0;
		}
	}
	// menu form show for add new or edit
	public function menuAction( $id = "" )
	{

		$menuData = array();
		if( $id != "" )
		{
			$data = $this->Menu_model->getMenu( $id );
			if( $data != false )
			{
				$menuData = $data;
			}
		}
		
		$viewData['menuData'] = $menuData;
		$header[ 'title' ] = "Add User | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
		$this->load->view('admin/menu/action' , $viewData);
		$this->load->view( "admin/templat/footer", $footer );
	}

	// form action
	public function formAction()
	{
		$response = array( 'success' => false, 'message' => array() );
		$this->form_validation->set_rules('menuName', 'Name', 'trim|required');
		$this->form_validation->set_rules('menuPrice', 'Price', 'trim|required');
		$this->form_validation->set_rules('selectRestaurant', 'Restaurant', 'trim|required');
		$this->form_validation->set_error_delimiters( "<p class='text-danger'>","</p>" );
		if ($this->form_validation->run() == TRUE ) {
			# code...
			$response['success']  = true;
			$dataArray = array(
					'menu_name'           => $this->input->post( 'menuName' ),
					'menu_price'          => $this->input->post( 'menuPrice' ),
					'menu_rd_id'         => $this->input->post( 'selectRestaurant' )
					
				);
			if( $this->input->post( 'menuId' ) )
			{
				$dataArray['menu_id'] = $this->input->post( 'menuId' );
			}
			if( $this->Menu_model->actionMenu( $dataArray ) )
			{
				$response['message']['error'] = true;
			}else{
				$response['message']['error'] = false;
			}
			
		} else {
			# code...
			foreach ($this->input->post() as $key => $value) {
				# code...
				$response['message'][$key] = form_error( $key );
			}
		}
		echo json_encode( $response );
	}
	// delete menu
	public function deleteMenu()
	{
		$id = $this->input->post( 'menu_id' );
		if ( $this->Menu_model->deleteMenuById( $id ) )
		{
			echo 1;
		}else{
			echo 0;
		}
	}

}

/* End of file Menu.php */
/* Location: ./application/controllers/admin/menu/Menu.php */