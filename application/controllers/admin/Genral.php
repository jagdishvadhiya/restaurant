<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Genral extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('admin/Genral_model');
	}
	public function countCache()
	{

		$this->load->driver( 'cache', array( 'adapter' => 'apc', 'backup' => 'file' )	);
		if( !$this->cache->get( 'countrows' ) )
		{
			$count_cache = $this->getCount();
			$this->cache->save( 'countrows', $count_cache, 60 );
		}
		$showCache = null;
		$showCache = $this->cache->get( 'countrows' );
		echo json_encode( $showCache );
	}

	public function getCount()
	{
		$this->load->model('admin/Genral_model');
		$data = array(
			'item'        => $this->Genral_model->getCountByTable( 'item_master' ),
			'menu'        => $this->Genral_model->getCountByTable( 'menu_master' ),
			'order'       => $this->Genral_model->getCountByTable( 'order_master' ),
			'restaurant'  => $this->Genral_model->getCountByTable( 'restaurant_detail' ),
			'table'       => $this->Genral_model->getCountByTable( 'table_master' ),
			'user'        => $this->Genral_model->getCountByTable( 'user_master' )
		);
		return $data;
	}

	// permition ajax action
	public function permitionAction()
	{
		$response = array( 'success' => 'false', 'message' => '' );
		$user_id = $this->input->post( 'user_id' );
		$action = $this->input->post( 'action' );
		$value = $this->input->post( 'value' );

		if( $this->Genral_model->updatePermition( $user_id, $action, $value ) )
		{
			$response['success'] = true;
			// $response['message'] = $this->db->last_query();
		}
		// echo json_encode($this->input->post());
		echo json_encode($response);
	}

}

/* End of file Genral.php */
/* Location: ./application/controllers/admin/Genral.php */