<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		// $this->load->library('session');
		$this->load->model('ajax/Ajax_model');
    $this->load->model( 'admin/Restaurant_model' );
    $this->load->model( 'admin/Menu_model' );
    $this->load->model( 'admin/User_model' );
    $this->load->model( 'admin/Order_model' );
    $this->load->model( 'admin/Table_model' );
 
	}
  public function test()
  {
    print_r( $this->input->post() );
    $this->load->model("ajax/Usertable_model");  
           $fetch_data = $this->Usertable_model->make_datatables_query(); 
           echo $fetch_data;
  }
	public function emailCheck()
	{
		$email =  $this->input->post('email');
		// $this->Ajax_model->checkEmailExist( $email );
		if( $data = $this->Ajax_model->checkEmailExist( $email ) )
		{
			echo "1";
		}else{
			echo "0";
		}
	}
  public function getRestaurantName( $id = "" )
  {
    $data = $this->Restaurant_model->getRestaurantName( $id );
    if( $data != false && $data != null)
    {
        return $data;
    }else{
      return 'Super Admin';
    }
  }
   public function getRestaurantNameId()
  {
    $response = array('status' => false , "data" => array() );
    $data = $this->Restaurant_model->getRestaurantNameId();
    if( $data != false )
    {
        $response['status'] = true;
        $response['data'] = $data;

     }
     echo json_encode( $response );
  }
   public function getMenuNameId( $restaurant_id = "" )
  {
    $response = array('status' => false , "data" => array() );
    if( $restaurant_id != "" )
    {
        $data = $this->Menu_model->getMenuNameId('',$restaurant_id);
    }else{
         $data = $this->Menu_model->getMenuNameId();
    }
    
    if( $data != false )
    {
        $response['status'] = true;
        $response['data'] = $data;

     }
     echo json_encode( $response );
  }
  public function getMenuNameByItemId( $item_id = "" )
  {
    $data = $this->Menu_model->getMenuNameById( $item_id );
    if( $data != false && $data != null)
    {
        return $data;
    }else{
      return 'Super Admin';
    }
  }
   public function getMenuPriceBYId( $menu_id = "" )
  {
    $data = $this->Menu_model->getMenuPriceBYId( $menu_id );
    if( $data != false && $data != null)
    {
        echo $data;
    }
  }

  public function setUserNameByOrderIdAjax( $item_id = "" )
  {
    $data = $this->Order_model->getUserNameByOrderId( $item_id );
    if( $data != false && $data != null)
    {
        echo  $data['om_user_name'] ;
    }else{
      echo 'Super Admin';
    }
  }
  public function getUserBYRestaurantId( $restaurant_id = "" )
  {
    $response = array('status' => false , "data" => array() );
    $data = $this->User_model->getUserBYRestaurantId( $restaurant_id );
    if( $data != false && $data != null)
    {
        $response['status'] = true;
        $response['data'] = $data;

    }
    echo json_encode( $response );
  }
    public function getTableBYRestaurantId( $restaurant_id = "" )
  {
    $response = array('status' => false , "data" => array() );
    $data = $this->Table_model->getTableBYRestaurantId( $restaurant_id );
    if( $data != false && $data != null)
    {
        $response['status'] = true;
        $response['data'] = $data;

    }
    echo json_encode( $response );
  }
  public function setUserNameByOrderId( $item_id = "" )
  {
    $data = $this->Order_model->getUserNameByOrderId( $item_id );
    if( $data != false && $data != null)
    {
        return  $data['om_user_name'] ;
    }else{
      return 'Super Admin';
    }
  }

	public function fetch_user()
	{
		      
		       $this->load->model("ajax/Usertable_model");  
           $fetch_data = $this->Usertable_model->get_datatables(); 
           
           $data = array();  
           foreach($fetch_data as $row)  
           {  
            
                $sub_array = array();  
                $sub_array[] = $row->um_id;  
                $sub_array[] = $row->um_name;  
                $sub_array[] = $row->um_email;  
                $sub_array[] = $row->um_mobile; 
                $sub_array[] = $this->getRestaurantName( $row->um_rd_id );
                $sub_array[] = $row->um_type;  
                $sub_array[] = $row->um_inserted_date;  

                // $sub_array[] = '<i class="fas fa-eye text-success show" data-toggle="modal" data-target="#showmodel" id="'.$row->um_id.'"></i>
                //                 <i class="fas fa-user-edit text-primary edit"  id="'.$row->um_id.'"></i>
                //                 <i class="fas fa-trash-alt text-danger delete" id="'.$row->um_id.'"></i>
                               // ';  
                $actionButtons = '<i class="fas fa-eye text-success show" data-toggle="modal" data-target="#showmodel" id="'.$row->um_id.'"></i>';
                if(  getEditPermitionAjax()  == '1' )
                {
                  $actionButtons .= '<i class="fas fa-user-edit text-primary edit"  id="'.$row->um_id.'"></i>';
                }
                if(  getDeletePermitionAjax()  == '1' )
                {
                  $actionButtons .= '<i class="fas fa-trash-alt text-danger delete" id="'.$row->um_id.'"></i>';
                }
                $sub_array[] =  $actionButtons; 
                $data[] = $sub_array;  
           }  
           $output = array(  
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Usertable_model->count_all(),
                        "recordsFiltered" => $this->Usertable_model->count_filtered(),
                        "data" => $data,
           );  
           echo json_encode($output); 
	}

	public function fetch_restaurant()
	{
		
		$this->load->model("ajax/Restauranttable_model");  
           $fetch_data = $this->Restauranttable_model->get_datatables(); 
           $data = array();  
           foreach($fetch_data as $row)  
           {  
                $sub_array = array();  
                $sub_array[] = $row->rd_id;  
                $sub_array[] = $row->rd_restaurant_name;  
                $sub_array[] = $row->rd_restaurant_owner_name;  
                $sub_array[] = $row->rd_restaurant_email;  
                $sub_array[] = $row->rd_mobile_number;  
                $sub_array[] = word_limiter($row->rd_address, 4); 
                $sub_array[] = $row->rd_inserted_date;  
                // $sub_array[] = '<i class="fas fa-eye text-success pointer show" data-toggle="modal" data-target="#showmodel" id="'.$row->rd_id.'"></i>
                //                 <i class="fas fa-user-edit text-primary pointer edit"  id="'.$row->rd_id.'"></i>
                //                 <i class="fas fa-trash-alt text-danger pointer delete" id="'.$row->rd_id.'"></i>'; 
                $actionButtons = '<i class="fas fa-eye text-success show" data-toggle="modal" data-target="#showmodel" id="'.$row->rd_id.'"></i>';

                if(  getEditPermitionAjax()  == '1' )
                {
                  $actionButtons .= '<i class="fas fa-user-edit text-primary edit"  id="'.$row->rd_id.'"></i>';
                }
                if(  getDeletePermitionAjax()  == '1' )
                {
                  $actionButtons .= '<i class="fas fa-trash-alt text-danger delete" id="'.$row->rd_id.'"></i>';
                }
                $sub_array[] =  $actionButtons;  
                $data[] = $sub_array;  
           }  
           $output = array(  
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Restauranttable_model->count_all(),
                        "recordsFiltered" => $this->Restauranttable_model->count_filtered(),
                        "data" => $data, 
           );  
           echo json_encode($output); 
	}

	public function fetch_menu()
	{
		
		$this->load->model("ajax/Menutable_model");  
           $fetch_data = $this->Menutable_model->get_datatables(); 
           $data = array();  
           foreach($fetch_data as $row)  
           {  
                $sub_array = array();  
                $sub_array[] = $row->menu_id;  
                $sub_array[] = $row->menu_name;  
                $sub_array[] = $row->menu_price;  
                $sub_array[] = $this->getRestaurantName( $row->menu_rd_id );  
                $sub_array[] = $row->menu_inserted;  
                // $sub_array[] = '<i class="fas fa-eye text-success show" data-toggle="modal" data-target="#showmodel" id="'.$row->menu_id.'"></i>
                //                 <i class="fas fa-user-edit text-primary edit"  id="'.$row->menu_id.'"></i>
                //                 <i class="fas fa-trash-alt text-danger delete" id="'.$row->menu_id.'"></i>
                //                '; 
                $actionButtons = '<i class="fas fa-eye text-success show" data-toggle="modal" data-target="#showmodel" id="'.$row->menu_id.'"></i>';
                
                if(  getEditPermitionAjax()  == '1' )
                {
                  $actionButtons .= '<i class="fas fa-user-edit text-primary edit"  id="'.$row->menu_id.'"></i>';
                }
                if(  getDeletePermitionAjax()  == '1' )
                {
                  $actionButtons .= '<i class="fas fa-trash-alt text-danger delete" id="'.$row->menu_id.'"></i>';
                }
                $sub_array[] =  $actionButtons;  
                $data[] = $sub_array;  
           }  
           $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->Menutable_model->count_all(),  
                "recordsFiltered"     =>     $this->Menutable_model->count_filtered(),  
                "data"                    =>     $data  
           );  
           echo json_encode($output); 
	}
	public function fetch_table()
	{
		
		$this->load->model("ajax/Tabletable_model");  
           $fetch_data = $this->Tabletable_model->get_datatables(); 
           $data = array();  
           foreach($fetch_data as $row)  
           {  
                $sub_array = array();  
                $sub_array[] = $row->tm_id;  
                $sub_array[] = $row->tm_name;  
                $sub_array[] = $this->getRestaurantName( $row->tm_rd_id );  
                $sub_array[] = $row->inserted_date;  
                // $sub_array[] = '<i class="fas fa-eye text-success show" data-toggle="modal" data-target="#showmodel" id="'.$row->tm_id.'"></i>
                //                 <i class="fas fa-user-edit text-primary edit"  id="'.$row->tm_id.'"></i>
                //                 <i class="fas fa-trash-alt text-danger delete" id="'.$row->tm_id.'"></i>
                //                ';  
                $actionButtons = '<i class="fas fa-eye text-success show" data-toggle="modal" data-target="#showmodel" id="'.$row->tm_id.'"></i>';
                
                if(  getEditPermitionAjax()  == '1' )
                {
                  $actionButtons .= '<i class="fas fa-user-edit text-primary edit"  id="'.$row->tm_id.'"></i>';
                }
                if(  getDeletePermitionAjax()  == '1' )
                {
                  $actionButtons .= '<i class="fas fa-trash-alt text-danger delete" id="'.$row->tm_id.'"></i>';
                }
                $sub_array[] =  $actionButtons;  
                $data[] = $sub_array;  
           }  
           $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->Tabletable_model->count_all(),  
                "recordsFiltered"     =>     $this->Tabletable_model->count_filtered(),  
                "data"                    =>     $data  
           );  
           echo json_encode($output); 
	}
	public function fetch_order()
	{
		
		$this->load->model("ajax/Ordertable_model");  
           $fetch_data = $this->Ordertable_model->get_datatables(); 
           // print_r($fetch_data);
           // exit();
           $data = array();  
           foreach($fetch_data as $row)  
           {  
            $orderQtyLink = base_url("admin/item/item/itemByOrderId/$row->om_id" );
            
                $sub_array = array();  
                $sub_array[] = $row->om_id;  
                $sub_array[] = $row->om_date;  
                $sub_array[] = $row->om_table_name;  
                $sub_array[] = $row->om_customer_name;  
                $sub_array[] = $row->om_mobile;  
                $sub_array[] = '<a href='.$orderQtyLink.' >'.$row->om_total_item.'</a>';  
                $sub_array[] = $row->om_total_amount;  
                $sub_array[] = $this->getRestaurantName( $row->om_rd_id );  
                $sub_array[] = $row->om_status;  
                $sub_array[] = $row->om_user_name;  
                $sub_array[] = $row->om_inserted_date;  
                // $sub_array[] = '<i class="fas fa-eye text-success show" data-toggle="modal" data-target="#showmodel" id="'.$row->om_id.'"></i>
                //                 <i class="fas fa-user-edit text-primary edit"  id="'.$row->om_id.'"></i>
                //                 <i class="fas fa-trash-alt text-danger delete" id="'.$row->om_id.'"></i>
                //                ';  
                $actionButtons = '<i class="fas fa-eye text-success show" data-toggle="modal" data-target="#showmodel" id="'.$row->om_id.'"></i>';
                
                if(  getEditPermitionAjax()  == '1' )
                {
                  $actionButtons .= '<i class="fas fa-user-edit text-primary edit"  id="'.$row->om_id.'"></i>';
                }
                if(  getDeletePermitionAjax()  == '1' )
                {
                  $actionButtons .= '<i class="fas fa-trash-alt text-danger delete" id="'.$row->om_id.'"></i>';
                }
                $sub_array[] =  $actionButtons;   
                $data[] = $sub_array;  
           }  
           $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->Ordertable_model->count_all(),  
                "recordsFiltered"     =>     $this->Ordertable_model->count_filtered(),  
                "data"                    =>     $data  
           );  
           echo json_encode($output); 
	}
	public function fetch_item()
	{
		
		$this->load->model("ajax/Itemtable_model");  
           $fetch_data = $this->Itemtable_model->get_datatables(); 
           $data = array();  
           foreach($fetch_data as $row)  
           {  
            $orderQtyLink = base_url("admin/item/item/itemByOrderId/$row->im_order_id" );
                $sub_array = array();  
                // $sub_array[] = $this->db->last_query();
                $sub_array[] = $row->im_id;
                $sub_array[] = $row->im_name;  
                $sub_array[] = $this->getMenuNameByItemId( $row->im_item_id );  
                $sub_array[] = '<a href='.$orderQtyLink.' >'.$row->im_qty.'</a>';  
                $sub_array[] = $row->im_amout;  
                $sub_array[] = $row->im_total_amout;  
                // $sub_array[] = $row->im_order_id;  
                $sub_array[] = $this->setUserNameByOrderId( $row->im_order_id );
                $sub_array[] = $row->im_inserted_date;  
                // $sub_array[] = '<i class="fas fa-eye text-success show" data-toggle="modal" data-target="#showmodel" id="'.$row->im_id.'"></i>
                //                 <i class="fas fa-user-edit text-primary edit"  id="'.$row->im_id.'"></i>
                //                 <i class="fas fa-trash-alt text-danger delete" id="'.$row->im_id.'"></i>
                //                ';
                $actionButtons = '<i class="fas fa-eye text-success show" data-toggle="modal" data-target="#showmodel" id="'.$row->im_id.'"></i>';
                
                if(  getEditPermitionAjax()  == '1' )
                {
                  $actionButtons .= '<i class="fas fa-user-edit text-primary edit"  id="'.$row->im_id.'"></i>';
                }
                if(  getDeletePermitionAjax()  == '1' )
                {
                  $actionButtons .= '<i class="fas fa-trash-alt text-danger delete" id="'.$row->im_id.'"></i>';
                }
                $sub_array[] =  $actionButtons;    
                $data[] = $sub_array;  
           }  
           $output = array(  
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Itemtable_model->count_all(),
                        "recordsFiltered" => $this->Itemtable_model->count_filtered(),
                        "data" => $data,
           );  
           echo json_encode($output); 
	}

}

/* End of file Ajax.php */
/* Location: ./application/controllers/admin/Ajax.php */