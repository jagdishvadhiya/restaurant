<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	if( ! $this->session->userdata( "login" ) )
		{
			redirect( 'admin/login' ,'refresh');
		}
		$this->load->model( "admin/Order_model" );
	}
	public function index()
	{
		$orderdata = array();
		$data = $this->Order_model->getOrder();
		if( $data != false )
		{
			$orderdata = $data;
		}
		$orderview[ "orderdata" ] = $orderdata;
		$header[ 'title' ] = "Order | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
		$this->load->view('admin/order/order', $orderview);	
		$this->load->view( "admin/templat/footer", $footer );
	}

	public function getOrderById()
	{
		$id = $this->input->post( 'order_id' );
		$data = $this->Order_model->getOrder( $id );
		$this->load->model('admin/Item_model');
		$data_item = $this->Item_model->getItemsByOrderId( $data[0]['om_id'] );
		$data[0]['items'] = $data_item;
		$this->load->model('admin/Restaurant_model');
		$data_restaurant = $this->Restaurant_model->getRestaurant( $data[0]['om_rd_id'] );
		$data[0]['restaurant_name'] = $data_restaurant[0]['rd_restaurant_name'];
		if( $data != false )
		{
			echo json_encode( $data[0] );
		}else{
			echo 0;
		}
	}

	// order form show for add new or edit
	public function orderAction( $id = "" )
	{

		$orderData = array();
		if( $id != "" )
		{
			$data = $this->Order_model->getorder( $id );
			if( $data != false )
			{
				$orderData = $data;
			}
		}
		
		$viewData['orderData'] = $orderData;
		$header[ 'title' ] = "Add User | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
		$this->load->view('admin/order/action' , $viewData);
		$this->load->view( "admin/templat/footer", $footer );
	}

	// form action
	public function formAction()
	{
		// $data = $this->input->post();
		$items = $this->input->post('itemData');
		$totalAmout = 0;
		foreach ($items as $key => $value) {
			# code...
			$totalAmout += $value['itemTotalPrice'];
		}
		$totalItems = count($items);
		$response = array( 'success' => false, 'message' => "" );
		$dataArray = array(
			'om_date'				 => date("Y-m-d H:i:m"),
			'om_table_id'            => $this->input->post( 'tableId' ),
			'om_table_name'          => $this->input->post( 'tableName' ),
			'om_customer_name'       => $this->input->post( 'cname' ),
			'om_mobile'              => $this->input->post( 'cmnumber' ),
			'om_total_amount'  		 => $totalAmout,
			'om_total_item' 		 => $totalItems,
			'om_rd_id' 				 => $this->input->post( 'restaurantSelect' ),
			'om_status' 			 => $this->input->post( 'ostatus' ),
			'om_user_id' 			 => $this->input->post( 'userSelect' ),
			'om_user_name' 			 => $this->input->post( 'userName' ),
			'items'                  => $items
			
		);
		if( $this->input->post( 'orderId' )  )
		{
			$dataArray['om_id'] = $this->input->post( 'orderId' );
		}
		if( $this->Order_model->orderAction( $dataArray ) ) 
		{
			$response['success'] = true;

		}
		// print_r($dataArray);
		echo json_encode( $response );
		// echo json_encode( $data );
	}

	// delete Order
	public function deleteOrder()
	{
		$id = $this->input->post( 'order_id' );
		if ( $this->Order_model->deleteOrderById( $id ) )
		{
			echo 1;
		}else{
			echo 0;
		}
	}
}

/* End of file Order.php */
/* Location: ./application/controllers/admin/order/Order.php */