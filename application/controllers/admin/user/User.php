<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if( ! $this->session->userdata( "login" ) )
		{
			redirect( 'admin/login' ,'refresh');
		}
		$this->load->model( "admin/User_model" );
		$this->load->model( 'admin/Restaurant_model' );
	}
	public function index()
	{
		 // $this->load->model( 'admin/Restaurant_model' );
		 // $this->Restaurant_model->getRestaurantName( '0' );
		 // exit;
		$userdata = array();
		$data = $this->User_model->getUser();
		if( $data != false )
		{
			$userdata = $data;
		}
		$userview[ "userdata" ] = $userdata;
		$header[ 'title' ] = "User | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
			$this->load->view('admin/user/user', $userview);
		$this->load->view( "admin/templat/footer", $footer );
	}
	// user form show for add new or edit
	public function userAction( $id = "" )
	{

		$userData = array();
		if( $id != "" )
		{
			$data = $this->User_model->getUser( $id );
			if( $data != false )
			{
				$userData = $data;
			}
		}
		
		$viewData['userData'] = $userData;
		$header[ 'title' ] = "Add User | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
		$this->load->view('admin/user/action' , $viewData);
		$this->load->view( "admin/templat/footer", $footer );
	}
	public function deleteUser()
	{
		$id = $this->input->post( 'user_id' );
		if ( $this->User_model->deleteUserById( $id ) )
		{
			echo 1;
		}else{
			echo 0;
		}
	}
	public function getUserById()
	{
		$id = $this->input->post( 'user_id' );
		$data = $this->User_model->getUser( $id );
		$this->load->model('admin/Restaurant_model');
		$data_restaurant = $this->Restaurant_model->getRestaurantName( $data[0]['um_rd_id'] );
		$data[0]['restaurant_name'] = $data_restaurant;
		if( $data != false )
		{
			echo json_encode( $data[0] );
		}else{
			echo 0;
		}
	}

	// form action
	public function formAction()
	{
		$response = array( 'success' => false, 'message' => array() );
		$this->form_validation->set_rules('userName', 'Name', 'trim|required');
		$this->form_validation->set_rules('userEmail', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('userMobile', 'Mobile Number', 'trim|required');
		if( ! $this->input->post( 'userId' ) || $this->input->post( 'userPassword' ) )
		{
			$this->form_validation->set_rules('userPassword', 'Password', 'trim|required');
			$this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|matches[userPassword]');
		}
		$this->form_validation->set_rules('userType', 'fieldlabel', 'required');
		if( $this->input->post( 'userType' ) != "superadmin" && $this->input->post( 'userType' ) != "" )
		{
			$this->form_validation->set_rules('userRestaurant', 'fieldlabel', 'required');
		}
		$this->form_validation->set_error_delimiters( "<p class='text-danger'>","</p>" );
		if ($this->form_validation->run() == TRUE ) {
			# code...
			$response['success']  = true;
			$dataArray = array(
					'um_name'           => $this->input->post( 'userName' ),
					'um_email'          => $this->input->post( 'userEmail' ),
					'um_mobile'         => $this->input->post( 'userMobile' ),
					'um_password'       => md5( $this->input->post( 'userPassword' ) ),
					'um_type' 			=> $this->input->post( 'userType' )
					
				);
			if( $this->input->post( 'userRestaurant' )  )
			{
				$dataArray['um_rd_id'] = $this->input->post( 'userRestaurant' );
			}else{
				$dataArray['um_rd_id'] = "0";
			}
			if( $this->input->post( 'userId' ) )
			{
				unset( $dataArray['um_password'] );
				if( $this->input->post( 'userPassword' ) )
				{
					$dataArray['um_password'] = md5( $this->input->post( 'userPassword' ) );
				}
				$dataArray['um_id'] = $this->input->post( 'userId' );
			}
			if( $this->User_model->actionUser( $dataArray ) )
			{
				$response['message']['error'] = true;
			}else{
				$response['message']['error'] = false;
			}
			
		} else {
			# code...
			foreach ($this->input->post() as $key => $value) {
				# code...
				$response['message'][$key] = form_error( $key );
			}
		}
		echo json_encode( $response );
	}
}
/* End of file User.php */
/* Location: ./application/controllers/admin/User.php */