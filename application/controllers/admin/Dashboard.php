<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if( ! $this->session->userdata( "login" ) )
		{
			redirect( 'admin/login' ,'refresh');
		}
		//Do your magic here
	}
	public function index()
	{
		
		$header[ 'title' ] = "Dashborad | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
		$this->load->view('admin/dashboard');
		$this->load->view( "admin/templat/footer", $footer );
	}
	public function test()
	{
		echo getDeletePermition(3);
	}
	

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/admin/Dashboard.php */