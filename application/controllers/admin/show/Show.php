<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Show extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function index()
	{
		$header[ 'title' ] = "Show Details | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
		$this->load->view('admin/show/show');
		$this->load->view( "admin/templat/footer", $footer );
	}

}

/* End of file Show.php */
/* Location: ./application/controllers/admin/show/Show.php */