<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
			if( ! $this->session->userdata( "login" ) )
		{
			redirect( 'admin/login' ,'refresh');
		}
		$this->load->model( "admin/Item_model" );
	}
	public function index()
	{
		
		$header[ 'title' ] = "Item | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
		$this->load->view('admin/item/item');	
		$this->load->view( "admin/templat/footer", $footer );
	}

	public function getItemById()
	{
		$id = $this->input->post( 'item_id' );
		$data = $this->Item_model->getItem( $id );
		$this->load->model('admin/Order_model');
		$data_order = $this->Order_model->getOrder( $data[0]['im_order_id'] );
		$data[0]['user_name'] = $data_order[0]['om_user_name'];
		if( $data != false )
		{
			echo json_encode( $data[0] );
		}else{
			echo 0;
		}
	}
	public function itemByOrderId( $orderId = "" )
	{
		$itemOrderId = $orderId;
		
		$itemview[ "itemOrderId" ] = $itemOrderId;
		$header[ 'title' ] = "Item | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
		$this->load->view('admin/item/item', $itemview);	
		$this->load->view( "admin/templat/footer", $footer );
	}
	// item form show for add new or edit
	public function itemAction( $id = "" )
	{

		$itemData = array();
		$menuData = array();
		if( $id != "" )
		{
			$data = $this->Item_model->getItem( $id );
			if( $data != false )
			{
				$itemData = $data;
				$menuId = $itemData[0]['im_item_id'];
				$result = $this->Item_model->getMenuNameIdByMenuIdRes( $menuId );
				if( $result != false )
				{
					$menuData = $result;
				}
			}
		}

		$viewData['itemData'] = $itemData;
		$viewData['menuData'] = $menuData;
		$header[ 'title' ] = "Add Item | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
		$this->load->view('admin/item/action' , $viewData);
		$this->load->view( "admin/templat/footer", $footer );
	}

	// form action
	public function formAction()
	{
		$response = array( 'success' => false, 'message' => array() );
		$this->form_validation->set_rules('itemName', 'Name', 'trim|required');
		$this->form_validation->set_error_delimiters( "<p class='text-danger'>","</p>" );
		if ($this->form_validation->run() == TRUE ) {
			# code...
			$response['success']  = true;
			$dataArray = array(
					'im_name'           => $this->input->post( 'itemName' ),
					'im_item_id'          => $this->input->post( 'itemMenuId' ),
					'im_qty'         => $this->input->post( 'itemQty' ),
					'im_amout'         => $this->input->post( 'itemPrice' ),
					
					'im_total_amout'   => $this->input->post( 'itemQty' ) * $this->input->post( 'itemPrice' )
				);
			
			if( $this->input->post( 'itemId' ) )
			{
				$dataArray['im_id'] = $this->input->post( 'itemId' );
			}
			if( $this->Item_model->actionItem( $dataArray ) )
			{
				$response['message']['error'] = true;
			}else{
				$response['message']['error'] = false;
			}
			
		} else {
			# code...
			foreach ($this->input->post() as $key => $value) {
				# code...
				$response['message'][$key] = form_error( $key );
			}
		}
		echo json_encode( $response );
	}

	// delete Item
	public function deleteItem()
	{
		$id = $this->input->post( 'item_id' );

		if ( $this->Item_model->deleteItemById( $id ) )
		{
			echo 1;
		}else{
			echo 0;
		}
	}
	public function getItemsByOrderId( $order_id = "" )
	{
		$data = $this->Item_model->getItemsByOrderId( $order_id );
		echo json_encode( $data );
	}

}

/* End of file Item.php */
/* Location: ./application/controllers/admin/item/Item.php */