<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Login_model');

		
	}
	public function index()
	{
		if( $this->session->userdata( "login" ) )
		{
			redirect('admin/dashboard','refresh');
		}
		$header[ 'title' ] = "Restaurant Admin | Login";
		$this->load->view('admin/login', $header);	
	}
	public function login()
	{
		// login form validation
		if( $this->input->post() )
		{
			$post_data = array(
				'um_email'    => $this->input->post( "email" ),
				'um_password' => md5( $this->input->post( "password" ) )
			);
			$data = $this->Login_model->login( $post_data );
			if( $data != false )
			{
				// print_r($data);
				$session_data = array(
					'login'         => true,
					'user_type'     => $data[0]['um_type'],
					'user_id'       => $data[0]['um_id'],
					'user_name'     => $data[0]['um_name'],
					'restaurant_id' => $data[0]['um_rd_id']

				);
				$this->session->set_userdata( $session_data );
				echo true;
			}else{
				echo false;
			}
		}else{
			return redirect( 'admin/login/' );
		}
	}
	public function dashboard()
	{
		print_r( $this->session->userdata( ) );
			exit;
		$data[ 'title' ] = "Dashborad | Admin";
		$this->load->view('admin/dashboard', $data);	
	}
	public function logout()
	{
		$this->session->unset_userdata( "login" );
		$this->session->unset_userdata( "user_type" );
		$this->session->unset_userdata( "user_id" );
		$this->session->unset_userdata( "user_name" );
		$this->session->unset_userdata( "restaurant_id" );
		return redirect('admin/login','refresh');
		// if( $this->session->unset_userdata( "login" ) )
		
	}



}

/* End of file Login.php */
/* Location: ./application/controllers/admin/Login.php */