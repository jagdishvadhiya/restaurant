<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Restaurant extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if( ! $this->session->userdata( "login" ) )
		{
			redirect( 'admin/login' ,'refresh');
		}
		$this->load->model( "admin/Restaurant_model" );
	}
	public function index()
	{
		$restaurantdata = array();
		$data = $this->Restaurant_model->getRestaurant();
		if( $data != false )
		{
			$restaurantdata = $data;
		}
		$userview[ "restaurantdata" ] = $restaurantdata;
		$header[ 'title' ] = "Restaurant | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
			$this->load->view('admin/restaurant/restaurant', $userview);
		$this->load->view( "admin/templat/footer", $footer );
	}
	public function getRestaurantById()
	{
		$id = $this->input->post( 'restaurant_id' );
		$data = $this->Restaurant_model->getRestaurant( $id );
		if( $data != false )
		{
			echo json_encode( $data[0] );
		}else{
			echo 0;
		}
	}
	// add new restaurant
	public function restaurantAction( $id = "" )
	{
		
		$restaurantData = array();
		if( $id != "" )
		{
			$data = $this->Restaurant_model->getRestaurant( $id );
			if( $data != false )
			{
				$restaurantData = $data;
			}
		}
		
		$viewData['restaurantData'] = $restaurantData;
		$header[ 'title' ] = "Add Restaurant | Admin";
		$footer['segment'] = $this->uri->segment(2);
		$this->load->view( "admin/templat/header", $header );
			$this->load->view('admin/restaurant/action',$viewData );
		$this->load->view( "admin/templat/footer", $footer );
	}
	public function formAction()
	{

		$response = array( 'success' => false, 'message' => array() );
		$this->form_validation->set_rules('restaurantName', 'Restaurant Name', 'trim|required');
		$this->form_validation->set_rules('ownerName', 'Owner Name', 'trim|required');
		$this->form_validation->set_rules('ownerEmail', 'Owner Email', 'trim|required');
		$this->form_validation->set_rules('ownerMobile', 'Mobile Number', 'trim|required');
		$this->form_validation->set_rules('ownerAddress', 'Address', 'trim|required');
		if( ! $this->input->post( 'restaurantId' ) )
		{
			$this->form_validation->set_rules('ownerPassword', 'Password', 'trim|required');
			$this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|matches[ownerPassword]');
		}
		if(  $this->input->post( 'restaurantId' ) && $this->input->post( 'ownerPassword' ) )
		{
			$this->form_validation->set_rules('ownerPassword', 'Password', 'trim|required');
			$this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required|matches[ownerPassword]');
		}
		$this->form_validation->set_error_delimiters( "<p class='text-danger'>","</p>" );
		if( $this->form_validation->run() == TRUE ) {
			$response['success'] = true;
			# processing form 
			$dataArray = array(
					'rd_restaurant_name'       => $this->input->post( 'restaurantName' ),
					'rd_restaurant_owner_name' => $this->input->post( 'ownerName' ),
					'rd_restaurant_email'      => $this->input->post( 'ownerEmail' ),
					'rd_mobile_number'         => $this->input->post( 'ownerMobile' ),
					'rd_address' 			   => $this->input->post( 'ownerAddress' ),
					'um_password'			   => $this->input->post( 'ownerPassword' ),
					
				);
			if( $this->input->post( 'restaurantId' )  )
			{
				unset( $dataArray['um_password'] );
				if( $this->input->post( 'ownerPassword' ) )
				{
					$dataArray['um_password'] = $this->input->post( 'ownerPassword' );
				}
				$dataArray['rd_id'] = $this->input->post( 'restaurantId' );
			}
			if( $this->Restaurant_model->actionRestaurant( $dataArray ) )
			{
				$response['message']['error'] = true;
			}else{
				$response['message']['error'] = false;
			}
			
			
		} else {
			# code...
			foreach ($this->input->post() as $key => $value) {
				# code...
				$response['message'][$key] = form_error( $key );
			}
		}
		echo json_encode( $response );
	}
	// delete restaurant
	public function deleteRestaurant()
	{
		$id = $this->input->post( 'restaurant_id' );
		if ( $this->Restaurant_model->deleteRestaurantById( $id ) )
		{
			echo 1;
		}else{
			echo 0;
		}
	}
}
/* End of file Restaurant.php */
/* Location: ./application/controllers/restaurant/Restaurant.php */